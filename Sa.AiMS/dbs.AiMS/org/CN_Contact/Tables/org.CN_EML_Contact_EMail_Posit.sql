﻿CREATE TABLE [org].[CN_EML_Contact_EMail_Posit]
(
	[CN_EML_ID]					INT					NOT	NULL	IDENTITY(1, 1),
	[CN_EML_CN_ID]				INT					NOT	NULL	CONSTRAINT [FK__CN_EML_Contact_EMail_Posit_CN_EML_CN_ID]	REFERENCES [org].[CN_Contact]([CN_ID]),

	[CN_EML_Type_ID]			INT					NOT	NULL,
	[CN_EML_Address]			NVARCHAR(254)		NOT	NULL	CONSTRAINT [CL__CN_EML_Contact_EMail_Posit_Address]			CHECK([CN_EML_Address] <> ''),
																CONSTRAINT [CV__CN_EML_Contact_EMail_Posit_Address]			CHECK([org].[CN_EML_Contact_EMail_IsValid]([CN_EML_Address]) = 1),
	[CN_EML_Checksum]			AS BINARY_CHECKSUM([CN_EML_Type_ID], [CN_EML_Address])
								PERSISTED			NOT	NULL,
	[CN_EML_ChangedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__CN_EML_Contact_EMail_Posit_ChangedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[CN_EML_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__CN_EML_Contact_EMail_Posit_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__CN_EML_Contact_EMail_Posit_RowGuid]			UNIQUE NONCLUSTERED([CN_EML_RowGuid]),
	CONSTRAINT [PK__CN_EML_Contact_EMail_Posit]		PRIMARY KEY CLUSTERED([CN_EML_ID] ASC)
		WITH (FILLFACTOR = 95),
	CONSTRAINT [UK__CN_EML_Contact_EMail_Posit]		UNIQUE NONCLUSTERED([CN_EML_CN_ID] ASC, [CN_EML_ChangedAt] DESC, [CN_EML_Checksum] ASC)
		WITH (FILLFACTOR = 95)
);