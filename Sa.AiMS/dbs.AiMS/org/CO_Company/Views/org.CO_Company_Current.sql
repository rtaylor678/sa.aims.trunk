﻿CREATE VIEW [org].[CO_Company_Current]
WITH SCHEMABINDING
AS
SELECT
	[CO].[CO_ID],
	[CO].[CO_PositedBy],
	[CO].[CO_PositedAt],

	[CO].[CO_NME_ID],
	[CO].[CO_NME_CO_ID],

	[CO].[CO_NME_CompanyTag],
	[CO].[CO_NME_CompanyName],
	[CO].[CO_NME_CompanyDetail],
	
	[CO].[CO_NME_Checksum],
	[CO].[CO_NME_ChangedAt],
	[CO].[CO_NME_PositedBy],
	[CO].[CO_NME_PositedAt],
	[CO].[CO_NME_PositReliability],
	[CO].[CO_NME_PositReliable]

FROM [org].[CO_Company_Rewind](DEFAULT, DEFAULT, DEFAULT)	[CO];
GO

CREATE TRIGGER [org].[CO_Company_Current_Insert]
ON [org].[CO_Company_Current]
INSTEAD OF INSERT
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CO_ID] IS NOT NULL))
		RAISERROR('INSERT into the anchor identity column ''CO_ID'' is not allowed.', 16, 1);
		--	THROW 50000, N'INSERT into the anchor identity column ''CO_ID'' is not allowed.', 1;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CO_NME_ID] IS NOT NULL))
		RAISERROR('INSERT into the identity column ''CO_NME_ID'' is not allowed.', 16, 1) WITH NOWAIT;
		--	THROW 50000, N'INSERT into the identity column ''CO_NME_ID'' is not allowed.', 1;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CO_NME_CO_ID] IS NOT NULL))
		RAISERROR('INSERT into the foreign key identity column ''CO_NME_CO_ID'' is not allowed.', 16, 1) WITH NOWAIT;
		--	THROW 50000, N'INSERT into the foreign key identity column ''CO_NME_CO_ID'' is not allowed.', 1;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CO_NME_Checksum] IS NOT NULL))
		RAISERROR('INSERT into the computed column ''CO_NME_Checksum'' is not allowed.', 16, 1) WITH NOWAIT;
		--	THROW 50000, N'INSERT into the computed column ''CO_NME_Checksum'' is not allowed.', 1;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CO_NME_PositReliable] IS NOT NULL))
		RAISERROR('INSERT into the computed column ''CO_NME_PositReliable'' is not allowed.', 16, 1) WITH NOWAIT;
		--	THROW 50000, N'INSERT into the computed column ''CO_NME_PositReliable'' is not allowed.', 1;

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @Inserted TABLE
	(
		[ROW_ID]					INT					NOT	NULL,

		[CO_PositedBy]				INT					NOT	NULL,
		[CO_PositedAt]				DATETIMEOFFSET(7)	NOT	NULL,

		[CO_NME_CompanyTag]			VARCHAR(12)				NULL	CHECK([CO_NME_CompanyTag] <> ''),
		[CO_NME_CompanyName]		NVARCHAR(24)			NULL	CHECK([CO_NME_CompanyName] <> ''),
		[CO_NME_CompanyDetail]		NVARCHAR(48)			NULL	CHECK([CO_NME_CompanyDetail] <> ''),
		[CO_NME_Checksum]			AS BINARY_CHECKSUM([CO_NME_CompanyTag], [CO_NME_CompanyName], [CO_NME_CompanyDetail])
									PERSISTED			NOT	NULL,
		[CO_NME_ChangedAt]			DATETIMEOFFSET(7)	NOT	NULL,
		[CO_NME_PositedBy]			INT					NOT	NULL,
		[CO_NME_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL,
		[CO_NME_PositReliability]	TINYINT				NOT	NULL,

		PRIMARY KEY([ROW_ID] ASC)
	)

	INSERT INTO @Inserted
	(
		[ROW_ID],

		[CO_PositedBy],
		[CO_PositedAt],

		[CO_NME_CompanyTag],
		[CO_NME_CompanyName],
		[CO_NME_CompanyDetail],

		[CO_NME_ChangedAt],
		[CO_NME_PositedBy],
		[CO_NME_PositedAt],
		[CO_NME_PositReliability]
	)
	SELECT
		ROW_NUMBER() OVER (PARTITION BY [i].[CO_ID] ORDER BY [i].[CO_ID]),

		COALESCE([i].[CO_PositedBy], @PositedBy),
		COALESCE([i].[CO_PositedAt], @Now),

		[i].[CO_NME_CompanyTag],
		[i].[CO_NME_CompanyName],
		[i].[CO_NME_CompanyDetail],

		COALESCE([i].[CO_NME_ChangedAt], @Now),
		COALESCE([i].[CO_NME_PositedBy], @PositedBy),
		COALESCE([i].[CO_NME_PositedAt], @Now),
		COALESCE([i].[CO_NME_PositReliability], @Reliability)

	FROM
		[inserted]	[i]
	LEFT OUTER JOIN
		[org].[CO_Company_Current]	[c]
			ON	[c].[CO_NME_Checksum]	= BINARY_CHECKSUM([i].[CO_NME_CompanyTag], [i].[CO_NME_CompanyName], [i].[CO_NME_CompanyDetail])
	WHERE	[c].[CO_NME_CO_ID]	IS NULL;

	DECLARE @Anchor TABLE
	(
		[ROW_ID]					INT		NOT	NULL	IDENTITY(1, 1),
		[CO_ID]						INT		NOT	NULL,
		PRIMARY KEY([ROW_ID] ASC),
		UNIQUE CLUSTERED([CO_ID] ASC)
	);

	INSERT INTO [org].[CO_Company]
	(
		[CO_PositedBy],
		[CO_PositedAt]
	)
	OUTPUT
		[inserted].[CO_ID]
	INTO
		@Anchor
		(
			[CO_ID]
		)
	SELECT
		[i].[CO_PositedBy],
		[i].[CO_PositedAt]
	FROM
		@Inserted	[i]
	ORDER BY
		[i].[ROW_ID] ASC;

	DECLARE @NME_Posit TABLE
	(
		[CO_NME_ID]					INT		NOT	NULL,
		[CO_NME_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([CO_NME_ID] ASC),
		UNIQUE CLUSTERED([CO_NME_Checksum] ASC)
	);

	INSERT INTO [org].[CO_NME_Company_Name_Posit]
	(
		[CO_NME_CO_ID],

		[CO_NME_CompanyTag],
		[CO_NME_CompanyName],
		[CO_NME_CompanyDetail],

		[CO_NME_ChangedAt]
	)
	OUTPUT
		[inserted].[CO_NME_ID],
		[inserted].[CO_NME_Checksum]
	INTO
		@NME_Posit
		(
			[CO_NME_ID],
			[CO_NME_Checksum]
		)
	SELECT
		[a].[CO_ID],

		[i].[CO_NME_CompanyTag],
		[i].[CO_NME_CompanyName],
		[i].[CO_NME_CompanyDetail],

		[i].[CO_NME_ChangedAt]
	FROM
		@Inserted		[i]
	INNER JOIN
		@Anchor			[a]
			ON	[a].[ROW_ID]	= [i].[ROW_ID];

	INSERT INTO [org].[CO_NME_Company_Name_Annex]
	(
		[CO_NME_ID],
		[CO_NME_PositedBy],
		[CO_NME_PositedAt],
		[CO_NME_PositReliability]
	)
	SELECT
		[p].[CO_NME_ID],
		[i].[CO_NME_PositedBy],
		[i].[CO_NME_PositedAt],
		[i].[CO_NME_PositReliability]
	FROM
		@Inserted		[i]
	INNER JOIN
		@NME_Posit		[p]
			ON	[p].[CO_NME_Checksum]	= [i].[CO_NME_Checksum];

END;
GO

CREATE TRIGGER [org].[CO_Company_Current_Update]
ON [org].[CO_Company_Current]
INSTEAD OF UPDATE
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(UPDATE([CO_ID]))
		RAISERROR('Cannot update the anchor identity column ''CO_ID''.', 16, 1);
		--	THROW 50000, N'Cannot update the anchor identity column ''CO_ID''.', 1;

	IF(UPDATE([CO_PositedBy]))
		RAISERROR('Cannot update the anchor column ''CO_PositedBy''.', 16, 1);
		--	THROW 50000, N'Cannot update the anchor column ''CO_PositedBy''.', 1;

	IF(UPDATE([CO_PositedAt]))
		RAISERROR('Cannot update the anchor column ''CO_PositedAt''.', 16, 1);
		--	THROW 50000, N'Cannot update the anchor column ''CO_PositedAt''.', 1;

	IF(UPDATE([CO_NME_ID]))
		RAISERROR('Cannot update the identity column ''CO_NME_ID''.', 16, 1);
		--	THROW 50000, N'Cannot update the identity column ''CO_NME_ID''.', 1;

	IF(UPDATE([CO_NME_CO_ID]))
		RAISERROR('Cannot update the foreign key identity column ''CO_NME_CO_ID''.', 16, 1);
		--	THROW 50000, N'Cannot update the foreign key identity column ''CO_NME_CO_ID''.', 1;

	IF(UPDATE([CO_NME_Checksum]))
		RAISERROR('Cannot update the computed column ''CO_NME_Checksum''.', 16, 1);
		--	THROW 50000, N'Cannot update the computed column ''CO_NME_Checksum''.', 1;

	IF(UPDATE([CO_NME_PositReliable]))
		RAISERROR('Cannot update the computed column ''CO_NME_PositReliable''.', 16, 1);
		--	THROW 50000, N'Cannot update the computed column ''CO_NME_PositReliable''.', 1;

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @NME_Posit TABLE
	(
		[CO_NME_ID]					INT		NOT	NULL,
		[CO_NME_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([CO_NME_ID] ASC),
		UNIQUE CLUSTERED([CO_NME_Checksum] ASC)
	);

	INSERT INTO [org].[CO_NME_Company_Name_Posit]
	(
		[CO_NME_CO_ID],

		[CO_NME_CompanyTag],
		[CO_NME_CompanyName],
		[CO_NME_CompanyDetail],

		[CO_NME_ChangedAt]
	)
	OUTPUT
		[inserted].[CO_NME_ID],
		[inserted].[CO_NME_Checksum]
	INTO
		@NME_Posit
		(
			[CO_NME_ID],
			[CO_NME_Checksum]
		)
	SELECT
		[i].[CO_NME_CO_ID],

		[i].[CO_NME_CompanyTag],
		[i].[CO_NME_CompanyName],
		[i].[CO_NME_CompanyDetail],

		[i].[CO_NME_ChangedAt] 
	FROM
		[inserted]		[i]
	WHERE	UPDATE([CO_NME_CompanyTag])
		OR	UPDATE([CO_NME_CompanyName])
		OR	UPDATE([CO_NME_CompanyDetail]);

	INSERT INTO [org].[CO_NME_Company_Name_Annex]
	(
		[CO_NME_ID],
		[CO_NME_PositedBy],
		[CO_NME_PositedAt],
		[CO_NME_PositReliability]
	)
	SELECT
		[p].[CO_NME_ID],
		COALESCE([i].[CO_NME_PositedBy], @PositedBy),
		COALESCE([i].[CO_NME_PositedAt], @Now),
		COALESCE([i].[CO_NME_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@NME_Posit		[p]
			ON	[p].[CO_NME_Checksum]	= BINARY_CHECKSUM([i].[CO_NME_CompanyTag], [i].[CO_NME_CompanyName], [i].[CO_NME_CompanyDetail]);

	INSERT INTO [org].[CO_NME_Company_Name_Annex]
	(
		[CO_NME_ID],
		[CO_NME_PositedBy],
		[CO_NME_PositedAt],
		[CO_NME_PositReliability]
	)
	SELECT
		[i].[CO_NME_ID],
		COALESCE([i].[CO_NME_PositedBy], @PositedBy),
		COALESCE([i].[CO_NME_PositedAt], @Now),
		COALESCE([i].[CO_NME_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	WHERE	[i].[CO_NME_ID]	IS NOT NULL
		AND	NOT(UPDATE([CO_NME_CompanyTag])
		OR	UPDATE([CO_NME_CompanyName])
		OR	UPDATE([CO_NME_CompanyDetail]));

END;
GO

CREATE TRIGGER [org].[CO_Company_Current_Delete]
ON [org].[CO_Company_Current]
INSTEAD OF DELETE
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	INSERT INTO [org].[CO_NME_Company_Name_Annex]
	(
		[CO_NME_ID],
		[CO_NME_PositedBy],
		[CO_NME_PositedAt],
		[CO_NME_PositReliability]
	)
	SELECT
		[d].[CO_NME_ID],
		@PositedBy,
		@Now,
		0
	FROM
		[deleted]	[d]
	WHERE
		[d].[CO_NME_ID]	IS NOT NULL;

END;
GO