﻿CREATE FUNCTION [org].[CN_EML_Contact_EMail_Rewind]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@Reliable		BIT					= 1
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[EML].[CN_EML_ID],
	[EML].[CN_EML_CN_ID],

	[EML].[CN_EML_Type_ID],
	[EML].[CN_EML_Address],

	[EML].[CN_EML_Checksum],
	[EML].[CN_EML_ChangedAt],
	[EML].[CN_EML_PositedBy],
	[EML].[CN_EML_PositedAt],
	[EML].[CN_EML_PositReliability],
	[EML].[CN_EML_PositReliable]

FROM
	[org].[CN_EML_Contact_EMail_RW](@ChangedBefore, @PositedBefore)			[EML]

WHERE	[EML].[CN_EML_PositReliable]	= @Reliable
	AND	[EML].[CN_EML_ID]				= (
		SELECT TOP 1
			[EML_s].[CN_EML_ID]
		FROM
			[org].[CN_EML_Contact_EMail_RW](@ChangedBefore, @PositedBefore)	[EML_s]
		WHERE
			[EML_s].[CN_EML_CN_ID]		= [EML].[CN_EML_CN_ID]
		ORDER BY
			[EML_s].[CN_EML_ChangedAt]	DESC,
			[EML_s].[CN_EML_PositedAt]	DESC
		);