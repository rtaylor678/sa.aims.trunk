﻿CREATE FUNCTION [org].[CO_NME_Company_Name_Posit_RW]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[p].[CO_NME_ID],
	[p].[CO_NME_CO_ID],

	[p].[CO_NME_CompanyTag],
	[p].[CO_NME_CompanyName],
	[p].[CO_NME_CompanyDetail],

	[p].[CO_NME_Checksum],
	[p].[CO_NME_ChangedAt]

FROM
	[org].[CO_NME_Company_Name_Posit]		[p]
WHERE
	[p].[CO_NME_ChangedAt] < @ChangedBefore;