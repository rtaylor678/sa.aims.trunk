﻿CREATE TABLE [org].[CO_NME_Company_Name_Annex]
(
	[CO_NME_ID]					INT					NOT	NULL	CONSTRAINT [FK__CO_NME_Company_Name_Annex_CO_NME_ID]		REFERENCES [org].[CO_NME_Company_Name_Posit]([CO_NME_ID]),

	[CO_NME_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__CO_NME_Company_Name_Annex_PositedBy]		DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__CO_NME_Company_Name_Annex_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[CO_NME_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__CO_NME_Company_Name_Annex_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[CO_NME_PositReliability]	TINYINT				NOT	NULL	CONSTRAINT [DF__CO_NME_Company_Name_Annex_PositRel]			DEFAULT(50),
	[CO_NME_PositReliable]		AS CONVERT(BIT, CASE WHEN [CO_NME_PositReliability] > 0 THEN 1 ELSE 0 END)
								PERSISTED			NOT	NULL,
	[CO_NME_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__CO_NME_Company_Name_Annex_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__CO_NME_Company_Name_Annex_RowGuid]			UNIQUE NONCLUSTERED([CO_NME_RowGuid]),
	CONSTRAINT [PK__CO_NME_Company_Name_Annex]	PRIMARY KEY CLUSTERED([CO_NME_ID] ASC, [CO_NME_PositedAt] DESC, [CO_NME_PositedBy] ASC)
		WITH (FILLFACTOR = 95)
);