﻿CREATE TABLE [dim].[CN_Country_LookUp]
(
	[CN_ID]						INT					NOT	NULL	IDENTITY(1, 1),

	[CN_CountryIso3]			CHAR(3)				NOT	NULL	CONSTRAINT [CL__CN_Countries_CountryIso3]		CHECK([CN_CountryIso3] <> ''),
	[CN_CountryName]			NVARCHAR(24)		NOT	NULL	CONSTRAINT [CL__CN_Countries_CountryName]		CHECK([CN_CountryName] <> ''),
																CONSTRAINT [UX__CN_Countries_CountryName]		UNIQUE NONCLUSTERED([CN_CountryName]),
	[CN_CountryDetail]			NVARCHAR(48)		NOT	NULL	CONSTRAINT [CL__CN_Countries_CountryDetail]		CHECK([CN_CountryDetail] <> ''),
																CONSTRAINT [UX__CN_Countries_CountryDetail]		UNIQUE NONCLUSTERED([CN_CountryDetail]),

	[CN_PositedBy]				INT					NOT	NULL	CONSTRAINT [DF__CN_Countries_PositedBy]			DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__CN_Countries_PositedBy]			REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[CN_PositedAt]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__CN_Countries_PositedAt]			DEFAULT(SYSDATETIMEOFFSET()),
	[CN_RowGuid]				UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__CN_Countries_CN_RowGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__CN_Countries_CN_RowGuid]		UNIQUE NONCLUSTERED([CN_RowGuid]),
	CONSTRAINT [PK__CN_Countries]	PRIMARY KEY([CN_ID] ASC),
	CONSTRAINT [UK__CN_Countries]	UNIQUE CLUSTERED([CN_CountryIso3] ASC),
);