﻿CREATE VIEW [org].[PE_NME_People_Name_SK]
WITH SCHEMABINDING
AS
SELECT
	[p].[PE_NME_ID],
	[p].[PE_NME_PE_ID],

	[p].[PE_NME_NamePrefix],
	[p].[PE_NME_NameFirst],
	[p].[PE_NME_NameMiddle],
	[p].[PE_NME_NameLast],
	[p].[PE_NME_NameSuffix],
	[p].[PE_NME_Title],

	[p].[PE_NME_Checksum],
	[p].[PE_NME_ChangedAt],
	[a].[PE_NME_PositedBy],
	[a].[PE_NME_PositedAt],
	[a].[PE_NME_PositReliability],
	[a].[PE_NME_PositReliable]

FROM
	[org].[PE_NME_People_Name_Posit]	[p]
INNER JOIN
	[org].[PE_NME_People_Name_Annex]	[a]
		ON	[a].[PE_NME_ID]		= [p].[PE_NME_ID];
GO

CREATE UNIQUE CLUSTERED INDEX [PK__PE_NME_People_Name_SK]
ON [org].[PE_NME_People_Name_SK]
(
	[PE_NME_PE_ID]				ASC,
	[PE_NME_ChangedAt]			DESC,
	[PE_NME_PositedAt]			DESC,
	[PE_NME_PositedBy]			ASC,
	[PE_NME_PositReliable]		ASC
)
WITH (FILLFACTOR = 95);
GO