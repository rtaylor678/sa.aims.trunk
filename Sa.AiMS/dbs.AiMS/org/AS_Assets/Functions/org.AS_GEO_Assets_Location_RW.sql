﻿CREATE FUNCTION [org].[AS_GEO_Assets_Location_RW]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[p].[AS_GEO_ID],
	[p].[AS_GEO_AS_ID],

	[p].[AS_GEO_Longitude_Deg],
	[p].[AS_GEO_Latitude_Deg],
	[p].[AS_GEO_Wgs84],

	[p].[AS_GEO_Checksum],
	[p].[AS_GEO_ChangedAt],
	[a].[AS_GEO_PositedBy],
	[a].[AS_GEO_PositedAt],
	[a].[AS_GEO_PositReliability],
	[a].[AS_GEO_PositReliable]

FROM
	[org].[AS_GEO_Assets_Location_Posit_RW](@ChangedBefore)				[p]

INNER JOIN
	[org].[AS_GEO_Assets_Location_Annex_RW](@PositedBefore)				[a]
		ON	[a].[AS_GEO_ID]				= [p].[AS_GEO_ID]
		AND	[a].[AS_GEO_PositedAt]		= (
			SELECT TOP 1
				[s].[AS_GEO_PositedAt]
			FROM
				[org].[AS_GEO_Assets_Location_Annex_RW](@PositedBefore)	[s]
			WHERE
				[s].[AS_GEO_ID]			= [a].[AS_GEO_ID]
			ORDER BY
				[s].[AS_GEO_PositedAt]	DESC
			);