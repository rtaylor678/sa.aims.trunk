﻿CREATE VIEW [org].[AS_Assets_Current]
WITH SCHEMABINDING
AS
SELECT
	[AS].[AS_ID],
	[AS].[AS_PositedBy],
	[AS].[AS_PositedAt],

	[AS].[AS_NME_ID],
	[AS].[AS_NME_AS_ID],

	[AS].[AS_NME_AssetTag],
	[AS].[AS_NME_AssetName],
	[AS].[AS_NME_AssetDetail],

	[AS].[AS_NME_Checksum],
	[AS].[AS_NME_ChangedAt],
	[AS].[AS_NME_PositedBy],
	[AS].[AS_NME_PositedAt],
	[AS].[AS_NME_PositReliability],
	[AS].[AS_NME_PositReliable],

	[AS].[AS_GEO_ID],
	[AS].[AS_GEO_AS_ID],

	[AS].[AS_GEO_Longitude_Deg],
	[AS].[AS_GEO_Latitude_Deg],
	[AS].[AS_GEO_Wgs84],

	[AS].[AS_GEO_Checksum],
	[AS].[AS_GEO_ChangedAt],
	[AS].[AS_GEO_PositedBy],
	[AS].[AS_GEO_PositedAt],
	[AS].[AS_GEO_PositReliability],
	[AS].[AS_GEO_PositReliable]

FROM [org].[AS_Assets_Rewind](DEFAULT, DEFAULT, DEFAULT)	[AS];
GO

CREATE TRIGGER [org].[AS_Assets_Current_Insert]
ON [org].[AS_Assets_Current]
INSTEAD OF INSERT
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[AS_ID] IS NOT NULL))
		RAISERROR('INSERT into the anchor identity column ''AS_ID'' is not allowed.', 16, 1) WITH NOWAIT;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[AS_NME_ID] IS NOT NULL))
		RAISERROR('INSERT into the identity column ''AS_NME_ID'' is not allowed.', 16, 1) WITH NOWAIT;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[AS_NME_AS_ID] IS NOT NULL))
		RAISERROR('INSERT into the foreign key identity column ''AS_NME_AS_ID'' is not allowed.', 16, 1) WITH NOWAIT;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[AS_NME_Checksum] IS NOT NULL))
		RAISERROR('INSERT into the computed column ''AS_NME_Checksum'' is not allowed.', 16, 1) WITH NOWAIT;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[AS_NME_PositReliable] IS NOT NULL))
		RAISERROR('INSERT into the computed column ''AS_NME_PositReliable'' is not allowed.', 16, 1) WITH NOWAIT;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[AS_GEO_ID] IS NOT NULL))
		RAISERROR('INSERT into the identity column ''AS_GEO_ID'' is not allowed.', 16, 1) WITH NOWAIT;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[AS_GEO_AS_ID] IS NOT NULL))
		RAISERROR('INSERT into the foreign key identity column ''AS_GEO_AS_ID'' is not allowed.', 16, 1) WITH NOWAIT;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[AS_GEO_Checksum] IS NOT NULL))
		RAISERROR('INSERT into the computed column ''AS_GEO_Checksum'' is not allowed.', 16, 1) WITH NOWAIT;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[AS_GEO_PositReliable] IS NOT NULL))
		RAISERROR('INSERT into the computed column ''AS_GEO_PositReliable'' is not allowed.', 16, 1) WITH NOWAIT;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[AS_GEO_Wgs84] IS NOT NULL))
		RAISERROR('INSERT into the computed column ''AS_GEO_Wgs84'' is not allowed.', 16, 1) WITH NOWAIT;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i]
		WHERE NOT (([i].[AS_GEO_Longitude_Deg]	IS NULL 	AND	[i].[AS_GEO_Latitude_Deg]	IS NULL)
			OR([i].[AS_GEO_Longitude_Deg]	IS NOT NULL	AND	[i].[AS_GEO_Latitude_Deg]	IS NOT NULL))))
		RAISERROR('''AS_GEO_Longitude_Deg'' and ''AS_GEO_Longitude_Deg'' must have values or both be NULL.', 16, 1) WITH NOWAIT;

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @Inserted TABLE
	(
		[ROW_ID]					INT					NOT	NULL,

		[AS_PositedBy]				INT					NOT	NULL,
		[AS_PositedAt]				DATETIMEOFFSET(7)	NOT	NULL,

		[AS_NME_AssetTag]			VARCHAR(12)			NOT	NULL	CHECK([AS_NME_AssetTag] <> ''),
		[AS_NME_AssetName]			NVARCHAR(24)		NOT	NULL	CHECK([AS_NME_AssetName] <> ''),
		[AS_NME_AssetDetail]		NVARCHAR(48)		NOT	NULL	CHECK([AS_NME_AssetDetail] <> ''),
		[AS_NME_Checksum]			AS BINARY_CHECKSUM([AS_NME_AssetTag], [AS_NME_AssetName], [AS_NME_AssetDetail])
									PERSISTED			NOT	NULL,
		[AS_NME_ChangedAt]			DATETIMEOFFSET(7)	NOT	NULL,
		[AS_NME_PositedBy]			INT					NOT	NULL,
		[AS_NME_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL,
		[AS_NME_PositReliability]	TINYINT				NOT	NULL,

		[AS_GEO_Longitude_Deg]		DECIMAL(17, 14)			NULL	CHECK([AS_GEO_Longitude_Deg] >= -180.0),	CHECK([AS_GEO_Longitude_Deg] <=  180.0),
		[AS_GEO_Latitude_Deg]		DECIMAL(17, 14)			NULL	CHECK([AS_GEO_Latitude_Deg] >= -90.0),		CHECK([AS_GEO_Latitude_Deg] <=  90.0),
		[AS_GEO_Checksum]			AS BINARY_CHECKSUM([AS_GEO_Longitude_Deg], [AS_GEO_Latitude_Deg])
									PERSISTED			NOT	NULL,
		[AS_GEO_ChangedAt]			DATETIMEOFFSET(7)	NOT	NULL,
		[AS_GEO_PositedBy]			INT					NOT	NULL,
		[AS_GEO_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL,
		[AS_GEO_PositReliability]	TINYINT				NOT	NULL,

		PRIMARY KEY([ROW_ID] ASC)
	);

	INSERT INTO @Inserted
	(
		[ROW_ID],

		[AS_PositedBy],
		[AS_PositedAt],

		[AS_NME_AssetTag],
		[AS_NME_AssetName],
		[AS_NME_AssetDetail],

		[AS_NME_ChangedAt],
		[AS_NME_PositedBy],
		[AS_NME_PositedAt],
		[AS_NME_PositReliability],

		[AS_GEO_Longitude_Deg],
		[AS_GEO_Latitude_Deg],

		[AS_GEO_ChangedAt],
		[AS_GEO_PositedBy],
		[AS_GEO_PositedAt],
		[AS_GEO_PositReliability]
	)
	SELECT
		ROW_NUMBER() OVER (PARTITION BY [i].[AS_ID] ORDER BY [i].[AS_ID]),

		COALESCE([i].[AS_PositedBy], @PositedBy),
		COALESCE([i].[AS_PositedAt], @Now),

		[i].[AS_NME_AssetTag],
		[i].[AS_NME_AssetName],
		[i].[AS_NME_AssetDetail],

		COALESCE([i].[AS_NME_ChangedAt], @Now),
		COALESCE([i].[AS_NME_PositedBy], @PositedBy),
		COALESCE([i].[AS_NME_PositedAt], @Now),
		COALESCE([i].[AS_NME_PositReliability], @Reliability),

		[i].[AS_GEO_Longitude_Deg],
		[i].[AS_GEO_Latitude_Deg],

		COALESCE([i].[AS_GEO_ChangedAt], @Now),
		COALESCE([i].[AS_GEO_PositedBy], @PositedBy),
		COALESCE([i].[AS_GEO_PositedAt], @Now),
		COALESCE([i].[AS_GEO_PositReliability], @Reliability)

	FROM
		[inserted]	[i]
	LEFT OUTER JOIN
		[org].[AS_Assets_Current]	[c]
			ON	[c].[AS_NME_Checksum]	= BINARY_CHECKSUM([i].[AS_NME_AssetTag], [i].[AS_NME_AssetName], [i].[AS_NME_AssetDetail])
	WHERE	[c].[AS_NME_AS_ID]	IS NULL;

	DECLARE @Anchor TABLE
	(
		[ROW_ID]					INT		NOT	NULL	IDENTITY(1, 1),
		[AS_ID]						INT		NOT	NULL,
		PRIMARY KEY([ROW_ID] ASC),
		UNIQUE CLUSTERED([AS_ID] ASC)
	);

	INSERT INTO [org].[AS_Assets]
	(
		[AS_PositedBy],
		[AS_PositedAt]
	)
	OUTPUT
		[inserted].[AS_ID]
	INTO
		@Anchor
		(
			[AS_ID]
		)
	SELECT
		[i].[AS_PositedBy],
		[i].[AS_PositedAt]
	FROM
		@Inserted	[i]
	ORDER BY
		[i].[ROW_ID] ASC;

	DECLARE @NME_Posit TABLE
	(
		[AS_NME_ID]					INT		NOT	NULL,
		[AS_NME_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([AS_NME_ID] ASC),
		UNIQUE CLUSTERED([AS_NME_Checksum] ASC)
	);

	INSERT INTO [org].[AS_NME_Assets_Name_Posit]
	(
		[AS_NME_AS_ID],

		[AS_NME_AssetTag],
		[AS_NME_AssetName],
		[AS_NME_AssetDetail],

		[AS_NME_ChangedAt]
	)
	OUTPUT
		[inserted].[AS_NME_ID],
		[inserted].[AS_NME_Checksum]
	INTO
		@NME_Posit
		(
			[AS_NME_ID],
			[AS_NME_Checksum]
		)
	SELECT
		[a].[AS_ID],

		[i].[AS_NME_AssetTag],
		[i].[AS_NME_AssetName],
		[i].[AS_NME_AssetDetail],

		[i].[AS_NME_ChangedAt] 
	FROM
		@Inserted		[i]
	INNER JOIN
		@Anchor			[a]
			ON	[a].[ROW_ID]	= [i].[ROW_ID];

	INSERT INTO [org].[AS_NME_Assets_Name_Annex]
	(
		[AS_NME_ID],
		[AS_NME_PositedBy],
		[AS_NME_PositedAt],
		[AS_NME_PositReliability]
	)
	SELECT
		[p].[AS_NME_ID],
		[i].[AS_NME_PositedBy],
		[i].[AS_NME_PositedAt],
		[i].[AS_NME_PositReliability]
	FROM
		@Inserted		[i]
	INNER JOIN
		@NME_Posit		[p]
			ON	[p].[AS_NME_Checksum]	= [i].[AS_NME_Checksum];

	DECLARE @GEO_Posit TABLE
	(
		[AS_GEO_ID]					INT		NOT	NULL,
		[AS_GEO_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([AS_GEO_ID] ASC),
		UNIQUE CLUSTERED([AS_GEO_Checksum] ASC)
	);

	INSERT INTO [org].[AS_GEO_Assets_Location_Posit]
	(
		[AS_GEO_AS_ID],
		[AS_GEO_Longitude_Deg],
		[AS_GEO_Latitude_Deg],
		[AS_GEO_ChangedAt]
	)
	OUTPUT
		[inserted].[AS_GEO_ID],
		[inserted].[AS_GEO_Checksum]
	INTO
		@GEO_Posit
		(
			[AS_GEO_ID],
			[AS_GEO_Checksum]
		)
	SELECT
		[a].[AS_ID],
		[i].[AS_GEO_Longitude_Deg],
		[i].[AS_GEO_Latitude_Deg],
		[i].[AS_NME_ChangedAt] 
	FROM
		@Inserted		[i]
	INNER JOIN
		@Anchor			[a]
			ON	[a].[ROW_ID]			= [i].[ROW_ID]
	WHERE	[i].[AS_GEO_Longitude_Deg]	IS NOT NULL
		AND	[i].[AS_GEO_Latitude_Deg]	IS NOT NULL;

	INSERT INTO [org].[AS_GEO_Assets_Location_Annex]
	(
		[AS_GEO_ID],
		[AS_GEO_PositedBy],
		[AS_GEO_PositedAt],
		[AS_GEO_PositReliability]
	)
	SELECT
		[p].[AS_GEO_ID],
		[i].[AS_GEO_PositedBy],
		[i].[AS_GEO_PositedAt],
		[i].[AS_GEO_PositReliability]
	FROM
		@Inserted		[i]
	INNER JOIN
		@GEO_Posit		[p]
			ON	[p].[AS_GEO_Checksum]	= [i].[AS_GEO_Checksum];

END;
GO

CREATE TRIGGER [org].[AS_Assets_Current_Update]
ON [org].[AS_Assets_Current]
INSTEAD OF UPDATE
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(UPDATE([AS_ID]))
		RAISERROR('Cannot update the anchor identity column ''AS_ID''.', 16, 1);

	IF(UPDATE([AS_PositedBy]))
		RAISERROR('Cannot update the anchor column ''AS_PositedBy''.', 16, 1);

	IF(UPDATE([AS_PositedAt]))
		RAISERROR('Cannot update the anchor column ''AS_PositedAt''.', 16, 1);

	IF(UPDATE([AS_NME_ID]))
		RAISERROR('Cannot update the identity column ''AS_NME_ID''.', 16, 1);

	IF(UPDATE([AS_NME_AS_ID]))
		RAISERROR('Cannot update the foreign key identity column ''AS_NME_AS_ID''.', 16, 1);

	IF(UPDATE([AS_NME_Checksum]))
		RAISERROR('Cannot update the computed column ''AS_NME_Checksum''.', 16, 1);

	IF(UPDATE([AS_NME_PositReliable]))
		RAISERROR('Cannot update the computed column ''AS_NME_PositReliable''.', 16, 1);

	IF(UPDATE([AS_GEO_ID]))
		RAISERROR('Cannot update the identity column ''AS_GEO_ID''.', 16, 1);

	IF(UPDATE([AS_GEO_AS_ID]))
		RAISERROR('Cannot update the foreign key identity column ''AS_GEO_AS_ID''.', 16, 1);

	IF(UPDATE([AS_GEO_Checksum]))
		RAISERROR('Cannot update the computed column ''AS_GEO_Checksum''.', 16, 1);

	IF(UPDATE([AS_GEO_PositReliable]))
		RAISERROR('Cannot update the computed column ''AS_GEO_PositReliable''.', 16, 1);

	IF(UPDATE([AS_GEO_Wgs84]))
		RAISERROR('Cannot update the computed column ''AS_GEO_Wgs84''.', 16, 1);

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i]
		WHERE NOT (([i].[AS_GEO_Longitude_Deg]	IS NULL 	AND	[i].[AS_GEO_Latitude_Deg]	IS NULL)
			OR([i].[AS_GEO_Longitude_Deg]	IS NOT NULL	AND	[i].[AS_GEO_Latitude_Deg]	IS NOT NULL))))
		RAISERROR('''AS_GEO_Longitude_Deg'' and ''AS_GEO_Longitude_Deg'' must have values or both be NULL.', 16, 1) WITH NOWAIT;

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @NME_Posit TABLE
	(
		[AS_NME_ID]					INT		NOT	NULL,
		[AS_NME_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([AS_NME_ID] ASC),
		UNIQUE CLUSTERED([AS_NME_Checksum] ASC)
	);

	INSERT INTO [org].[AS_NME_Assets_Name_Posit]
	(
		[AS_NME_AS_ID],

		[AS_NME_AssetTag],
		[AS_NME_AssetName],
		[AS_NME_AssetDetail],

		[AS_NME_ChangedAt]
	)
	OUTPUT
		[inserted].[AS_NME_ID],
		[inserted].[AS_NME_Checksum]
	INTO
		@NME_Posit
		(
			[AS_NME_ID],
			[AS_NME_Checksum]
		)
	SELECT
		[i].[AS_NME_AS_ID],

		[i].[AS_NME_AssetTag],
		[i].[AS_NME_AssetName],
		[i].[AS_NME_AssetDetail],

		COALESCE([i].[AS_NME_ChangedAt], @Now)
	FROM
		[inserted]		[i]
	WHERE	UPDATE([AS_NME_AssetTag])
		OR	UPDATE([AS_NME_AssetName])
		OR	UPDATE([AS_NME_AssetDetail]);

	INSERT INTO [org].[AS_NME_Assets_Name_Annex]
	(
		[AS_NME_ID],
		[AS_NME_PositedBy],
		[AS_NME_PositedAt],
		[AS_NME_PositReliability]
	)
	SELECT
		[p].[AS_NME_ID],
		COALESCE([i].[AS_NME_PositedBy], @PositedBy),
		COALESCE([i].[AS_NME_PositedAt], @Now),
		COALESCE([i].[AS_NME_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@NME_Posit		[p]
			ON	[p].[AS_NME_Checksum]	= BINARY_CHECKSUM([i].[AS_NME_AssetTag], [i].[AS_NME_AssetName], [i].[AS_NME_AssetDetail]);

	INSERT INTO [org].[AS_NME_Assets_Name_Annex]
	(
		[AS_NME_ID],
		[AS_NME_PositedBy],
		[AS_NME_PositedAt],
		[AS_NME_PositReliability]
	)
	SELECT
		[i].[AS_NME_ID],
		COALESCE([i].[AS_NME_PositedBy], @PositedBy),
		COALESCE([i].[AS_NME_PositedAt], @Now),
		COALESCE([i].[AS_NME_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	WHERE	[i].[AS_NME_ID]	IS NOT NULL
		AND	NOT(UPDATE([AS_NME_AssetTag])
			OR	UPDATE([AS_NME_AssetName])
			OR	UPDATE([AS_NME_AssetDetail]));

	DECLARE @GEO_Posit TABLE
	(
		[AS_GEO_ID]					INT		NOT	NULL,
		[AS_GEO_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([AS_GEO_ID] ASC),
		UNIQUE CLUSTERED([AS_GEO_Checksum] ASC)
	);

	INSERT INTO [org].[AS_GEO_Assets_Location_Posit]
	(
		[AS_GEO_AS_ID],
		[AS_GEO_Longitude_Deg],
		[AS_GEO_Latitude_Deg],
		[AS_GEO_ChangedAt]
	)
	OUTPUT
		[inserted].[AS_GEO_ID],
		[inserted].[AS_GEO_Checksum]
	INTO
		@GEO_Posit
		(
			[AS_GEO_ID],
			[AS_GEO_Checksum]
		)
	SELECT
		[i].[AS_GEO_AS_ID],
		[i].[AS_GEO_Longitude_Deg],
		[i].[AS_GEO_Latitude_Deg],
		COALESCE([i].[AS_GEO_ChangedAt], @Now)
	FROM
		[inserted]		[i]
	WHERE	UPDATE([AS_GEO_Longitude_Deg])
		OR	UPDATE([AS_GEO_Latitude_Deg])
		AND	[i].[AS_GEO_Longitude_Deg]	IS NOT NULL
		AND	[i].[AS_GEO_Latitude_Deg]	IS NOT NULL;

	INSERT INTO [org].[AS_GEO_Assets_Location_Annex]
	(
		[AS_GEO_ID],
		[AS_GEO_PositedBy],
		[AS_GEO_PositedAt],
		[AS_GEO_PositReliability]
	)
	SELECT
		[p].[AS_GEO_ID],
		COALESCE([i].[AS_GEO_PositedBy], @PositedBy),
		COALESCE([i].[AS_GEO_PositedAt], @Now),
		COALESCE([i].[AS_GEO_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@GEO_Posit		[p]
			ON	[p].[AS_GEO_Checksum]	= BINARY_CHECKSUM([i].[AS_GEO_Longitude_Deg], [i].[AS_GEO_Latitude_Deg]);

	INSERT INTO [org].[AS_GEO_Assets_Location_Annex]
	(
		[AS_GEO_ID],
		[AS_GEO_PositedBy],
		[AS_GEO_PositedAt],
		[AS_GEO_PositReliability]
	)
	SELECT
		[i].[AS_GEO_ID],
		COALESCE([i].[AS_GEO_PositedBy], @PositedBy),
		COALESCE([i].[AS_GEO_PositedAt], @Now),
		CASE WHEN	[i].[AS_GEO_Longitude_Deg]	IS NOT NULL
				AND	[i].[AS_GEO_Latitude_Deg]	IS NOT NULL
			THEN
				COALESCE([i].[AS_GEO_PositReliability], @Reliability)
			ELSE
				0
			END
	FROM
		[inserted]		[i]
	WHERE	[i].[AS_GEO_ID]				IS NOT NULL
		AND	NOT(UPDATE([AS_GEO_Longitude_Deg])
			OR	UPDATE([AS_GEO_Latitude_Deg]))
		OR	(	[i].[AS_GEO_Longitude_Deg]	IS NULL
			AND	[i].[AS_GEO_Latitude_Deg]	IS NULL);

END;
GO

CREATE TRIGGER [org].[AS_Assets_Current_Delete]
ON [org].[AS_Assets_Current]
INSTEAD OF DELETE
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	INSERT INTO [org].[AS_NME_Assets_Name_Annex]
	(
		[AS_NME_ID],
		[AS_NME_PositedBy],
		[AS_NME_PositedAt],
		[AS_NME_PositReliability]
	)
	SELECT
		[d].[AS_NME_ID],
		@PositedBy,
		@Now,
		0
	FROM
		[deleted]	[d]
	WHERE
		[d].[AS_NME_ID]	IS NOT NULL;

	INSERT INTO [org].[AS_GEO_Assets_Location_Annex]
	(
		[AS_GEO_ID],
		[AS_GEO_PositedBy],
		[AS_GEO_PositedAt],
		[AS_GEO_PositReliability]
	)
	SELECT
		[d].[AS_GEO_ID],
		@PositedBy,
		@Now,
		0
	FROM
		[deleted]	[d]
	WHERE
		[d].[AS_GEO_ID]	IS NOT NULL;

END;
GO