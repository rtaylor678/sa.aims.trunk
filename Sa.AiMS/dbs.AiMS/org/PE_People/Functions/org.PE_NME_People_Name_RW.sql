﻿CREATE FUNCTION [org].[PE_NME_People_Name_RW]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[p].[PE_NME_ID],
	[p].[PE_NME_PE_ID],

	[p].[PE_NME_NamePrefix],
	[p].[PE_NME_NameFirst],
	[p].[PE_NME_NameMiddle],
	[p].[PE_NME_NameLast],
	[p].[PE_NME_NameSuffix],
	[p].[PE_NME_Title],

	[p].[PE_NME_Checksum],
	[p].[PE_NME_ChangedAt],
	[a].[PE_NME_PositedBy],
	[a].[PE_NME_PositedAt],
	[a].[PE_NME_PositReliability],
	[a].[PE_NME_PositReliable]
FROM
	[org].[PE_NME_People_Name_Posit_RW](@ChangedBefore)				[p]

INNER JOIN
	[org].[PE_NME_People_Name_Annex_RW](@PositedBefore)				[a]
		ON	[a].[PE_NME_ID]				= [p].[PE_NME_ID]
		AND	[a].[PE_NME_PositedAt]		= (
			SELECT TOP 1
				[s].[PE_NME_PositedAt]
			FROM
				[org].[PE_NME_People_Name_Annex_RW](@PositedBefore)	[s]
			WHERE	[s].[PE_NME_ID]		= [a].[PE_NME_ID]
			ORDER BY
				[s].[PE_NME_PositedAt]	DESC
			);