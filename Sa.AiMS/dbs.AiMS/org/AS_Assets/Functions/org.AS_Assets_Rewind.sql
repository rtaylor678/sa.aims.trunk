﻿CREATE FUNCTION [org].[AS_Assets_Rewind]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@Reliable		BIT					= 1
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[AS].[AS_ID],
	[AS].[AS_PositedBy],
	[AS].[AS_PositedAt],

	[NME].[AS_NME_ID],
	[NME].[AS_NME_AS_ID],

	[NME].[AS_NME_AssetTag],
	[NME].[AS_NME_AssetName],
	[NME].[AS_NME_AssetDetail],

	[NME].[AS_NME_Checksum],
	[NME].[AS_NME_ChangedAt],
	[NME].[AS_NME_PositedBy],
	[NME].[AS_NME_PositedAt],
	[NME].[AS_NME_PositReliability],
	[NME].[AS_NME_PositReliable],

	[GEO].[AS_GEO_ID],
	[GEO].[AS_GEO_AS_ID],

	[GEO].[AS_GEO_Longitude_Deg],
	[GEO].[AS_GEO_Latitude_Deg],
	[GEO].[AS_GEO_Wgs84],

	[GEO].[AS_GEO_Checksum],
	[GEO].[AS_GEO_ChangedAt],
	[GEO].[AS_GEO_PositedBy],
	[GEO].[AS_GEO_PositedAt],
	[GEO].[AS_GEO_PositReliability],
	[GEO].[AS_GEO_PositReliable]

FROM
	[org].[AS_Assets]		[AS]

INNER JOIN
	[org].[AS_NME_Assets_Name_RW](@ChangedBefore, @PositedBefore)					[NME]
		ON	[NME].[AS_NME_AS_ID]			= [AS].[AS_ID]
		AND	[NME].[AS_NME_PositReliable]	= @Reliable
		AND	[NME].[AS_NME_ID]				= (
			SELECT TOP 1
				[NME_s].[AS_NME_ID]
			FROM
				[org].[AS_NME_Assets_Name_RW](@ChangedBefore, @PositedBefore)		[NME_s]
			WHERE
				[NME_s].[AS_NME_AS_ID]		= [NME].[AS_NME_AS_ID]
			ORDER BY
				[NME_s].[AS_NME_ChangedAt]	DESC,
				[NME_s].[AS_NME_PositedAt]	DESC
			)

LEFT OUTER JOIN
	[org].[AS_GEO_Assets_Location_RW](@ChangedBefore, @PositedBefore)				[GEO]
		ON	[GEO].[AS_GEO_AS_ID]			= [AS].[AS_ID]
		AND	[GEO].[AS_GEO_PositReliable]	= @Reliable
		AND	[GEO].[AS_GEO_ID]				= (
			SELECT TOP 1
				[GEO_s].[AS_GEO_ID]
			FROM
				[org].[AS_GEO_Assets_Location_RW](@ChangedBefore, @PositedBefore)	[GEO_s]
			WHERE
				[GEO_s].[AS_GEO_AS_ID]		= [GEO].[AS_GEO_AS_ID]
			ORDER BY
				[GEO_s].[AS_GEO_ChangedAt]	DESC,
				[GEO_s].[AS_GEO_PositedAt]	DESC
			);