﻿CREATE TABLE [org].[CN_Contact]
(
	[CN_ID]					INT					NOT	NULL	IDENTITY(1, 1),

	[CN_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__CN_Contact_PositedBy]	DEFAULT([posit].[Get_PositorId]())
															CONSTRAINT [FK__CN_Contact_PositedBy]	REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[CN_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__CN_Contact_PositedAt]	DEFAULT(SYSDATETIMEOFFSET()),
	[CN_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__CN_Contact_RowGuid]		DEFAULT(NEWSEQUENTIALID()) ROWGUIDCOL,
															CONSTRAINT [UX__CN_Contact_RowGuid]		UNIQUE([CN_RowGuid]),

	CONSTRAINT [PK__CN_Contact]	PRIMARY KEY CLUSTERED ([CN_ID] ASC)
		WITH (FILLFACTOR = 95)
);