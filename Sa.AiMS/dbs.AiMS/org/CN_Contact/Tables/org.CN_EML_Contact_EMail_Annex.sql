﻿CREATE TABLE [org].[CN_EML_Contact_EMail_Annex]
(
	[CN_EML_ID]					INT					NOT	NULL	CONSTRAINT [FK__CN_EML_Contact_EMail_Annex_CN_EML_ID]		REFERENCES [org].[CN_EML_Contact_EMail_Posit]([CN_EML_ID]),

	[CN_EML_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__CN_EML_Contact_EMail_Annex_PositedBy]		DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__CN_EML_Contact_EMail_Annex_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[CN_EML_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__CN_EML_Contact_EMail_Annex_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[CN_EML_PositReliability]	TINYINT				NOT	NULL	CONSTRAINT [DF__CN_EML_Contact_EMail_Annex_PositRel]		DEFAULT(50),
	[CN_EML_PositReliable]		AS CONVERT(BIT, CASE WHEN [CN_EML_PositReliability] > 0 THEN 1 ELSE 0 END)
								PERSISTED			NOT	NULL,
	[CN_EML_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__CN_EML_Contact_EMail_Annex_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__CN_EML_Contact_EMail_Annex_RowGuid]			UNIQUE NONCLUSTERED([CN_EML_RowGuid]),
	CONSTRAINT [PK__CN_EML_Contact_EMail_Annex]	PRIMARY KEY CLUSTERED([CN_EML_ID] ASC, [CN_EML_PositedAt] DESC, [CN_EML_PositedBy] ASC)
		WITH (FILLFACTOR = 95)
);