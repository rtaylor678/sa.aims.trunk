﻿CREATE FUNCTION [org].[CN_EML_Contact_EMail_RW]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[p].[CN_EML_ID],
	[p].[CN_EML_CN_ID],

	[p].[CN_EML_Type_ID],
	[p].[CN_EML_Address],

	[p].[CN_EML_Checksum],
	[p].[CN_EML_ChangedAt],
	[a].[CN_EML_PositedBy],
	[a].[CN_EML_PositedAt],
	[a].[CN_EML_PositReliability],
	[a].[CN_EML_PositReliable]

FROM
	[org].[CN_EML_Contact_EMail_Posit_RW](@ChangedBefore)				[p]

INNER JOIN
	[org].[CN_EML_Contact_EMail_Annex_RW](@PositedBefore)				[a]
		ON	[a].[CN_EML_ID]				= [p].[CN_EML_ID]
		AND	[a].[CN_EML_PositedAt]		= (
			SELECT TOP 1
				[s].[CN_EML_PositedAt]
			FROM
				[org].[CN_EML_Contact_EMail_Annex_RW](@PositedBefore)	[s]
			WHERE
				[s].[CN_EML_ID]			= [a].[CN_EML_ID]
			ORDER BY
				[s].[CN_EML_PositedAt]	DESC
			);