﻿CREATE FUNCTION [org].[CN_ADD_Contact_Address_Rewind]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@Reliable		BIT					= 1
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[ADD].[CN_ADD_ID],
	[ADD].[CN_ADD_CN_ID],

	[ADD].[CN_ADD_Type_ID],
	[ADD].[CN_ADD_Country_ID],
	[ADD].[CN_ADD_State],
	[ADD].[CN_ADD_City],
	[ADD].[CN_ADD_PostalCode],
	[ADD].[CN_ADD_PoBox],
	[ADD].[CN_ADD_Address1],
	[ADD].[CN_ADD_Address2],
	[ADD].[CN_ADD_Address3],

	[ADD].[CN_ADD_Checksum],
	[ADD].[CN_ADD_ChangedAt],
	[ADD].[CN_ADD_PositedBy],
	[ADD].[CN_ADD_PositedAt],
	[ADD].[CN_ADD_PositReliability],
	[ADD].[CN_ADD_PositReliable]

FROM
	[org].[CN_ADD_Contact_Address_RW](@ChangedBefore, @PositedBefore)			[ADD]

WHERE	[ADD].[CN_ADD_PositReliable]	= @Reliable
	AND	[ADD].[CN_ADD_ID]				= (
		SELECT TOP 1
			[ADD_s].[CN_ADD_ID]
		FROM
			[org].[CN_ADD_Contact_Address_RW](@ChangedBefore, @PositedBefore)	[ADD_s]
		WHERE
			[ADD_s].[CN_ADD_CN_ID]		= [ADD].[CN_ADD_CN_ID]
		ORDER BY
			[ADD_s].[CN_ADD_ChangedAt]	DESC,
			[ADD_s].[CN_ADD_PositedAt]	DESC
		);