﻿CREATE TABLE [org].[CO_Company]
(
	[CO_ID]					INT					NOT	NULL	IDENTITY(1, 1),

	[CO_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__CO_Company_PositedBy]	DEFAULT([posit].[Get_PositorId]())
															CONSTRAINT [FK__CO_Company_PositedBy]	REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[CO_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__CO_Company_PositedAt]	DEFAULT(SYSDATETIMEOFFSET()),
	[CO_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__CO_Company_RowGuid]		DEFAULT(NEWSEQUENTIALID()) ROWGUIDCOL,
															CONSTRAINT [UX__CO_Company_RowGuid]		UNIQUE([CO_RowGuid]),
	CONSTRAINT [PK__CO_Company]	PRIMARY KEY CLUSTERED ([CO_ID] ASC)
		WITH (FILLFACTOR = 95)
);