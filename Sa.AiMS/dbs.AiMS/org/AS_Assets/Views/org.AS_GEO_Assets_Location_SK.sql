﻿CREATE VIEW [org].[AS_GEO_Assets_Location_SK]
WITH SCHEMABINDING
AS
SELECT
	[p].[AS_GEO_ID],
	[p].[AS_GEO_AS_ID],

	[p].[AS_GEO_Longitude_Deg],
	[p].[AS_GEO_Latitude_Deg],
	[p].[AS_GEO_Wgs84],

	[p].[AS_GEO_Checksum],
	[p].[AS_GEO_ChangedAt],
	[a].[AS_GEO_PositedBy],
	[a].[AS_GEO_PositedAt],
	[a].[AS_GEO_PositReliability],
	[a].[AS_GEO_PositReliable]

FROM
	[org].[AS_GEO_Assets_Location_Posit]	[p]
INNER JOIN
	[org].[AS_GEO_Assets_Location_Annex]	[a]
		ON	[a].[AS_GEO_ID]		= [p].[AS_GEO_ID];
GO

CREATE UNIQUE CLUSTERED INDEX [PK__AS_GEO_Assets_Location_SK]
ON [org].[AS_GEO_Assets_Location_SK]
(
	[AS_GEO_AS_ID]				ASC,
	[AS_GEO_ChangedAt]			DESC,
	[AS_GEO_PositedAt]			DESC,
	[AS_GEO_PositedBy]			ASC,
	[AS_GEO_PositReliable]		ASC
)
WITH (FILLFACTOR = 95);
GO