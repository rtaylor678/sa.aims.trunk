﻿CREATE VIEW [org].[CN_EML_Contact_EMail_Current]
WITH SCHEMABINDING
AS
SELECT
	[EML].[CN_EML_ID],
	[EML].[CN_EML_CN_ID],

	[EML].[CN_EML_Type_ID],
	[EML].[CN_EML_Address],

	[EML].[CN_EML_Checksum],
	[EML].[CN_EML_ChangedAt],
	[EML].[CN_EML_PositedBy],
	[EML].[CN_EML_PositedAt],
	[EML].[CN_EML_PositReliability],
	[EML].[CN_EML_PositReliable]

FROM [org].[CN_EML_Contact_EMail_Rewind](DEFAULT, DEFAULT, DEFAULT)	[EML];
GO

CREATE TRIGGER [org].[CN_EML_Contact_EMail_Current_Insert]
ON [org].[CN_EML_Contact_EMail_Current]
INSTEAD OF INSERT
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CN_EML_ID] IS NOT NULL))
		RAISERROR('INSERT into the identity column ''CN_EML_ID'' is not allowed.', 16, 1) WITH NOWAIT;
		--	THROW 50000, N'INSERT into the identity column ''CN_EML_ID'' is not allowed.', 1;

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @EML_Posit TABLE
	(
		[CN_EML_ID]					INT		NOT	NULL,
		[CN_EML_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([CN_EML_ID] ASC),
		UNIQUE CLUSTERED([CN_EML_Checksum] ASC)
	);

	INSERT INTO [org].[CN_EML_Contact_EMail_Posit]
	(
		[CN_EML_CN_ID],

		[CN_EML_Type_ID],
		[CN_EML_Address],

		[CN_EML_ChangedAt]
	)
	OUTPUT
		[inserted].[CN_EML_ID],
		[inserted].[CN_EML_Checksum]
	INTO
		@EML_Posit
		(
			[CN_EML_ID],
			[CN_EML_Checksum]
		)
	SELECT
		[i].[CN_EML_CN_ID],

		[i].[CN_EML_Type_ID],
		[i].[CN_EML_Address],

		COALESCE([i].[CN_EML_ChangedAt], @Now)
	FROM
		[inserted]		[i];

	INSERT INTO [org].[CN_EML_Contact_EMail_Annex]
	(
		[CN_EML_ID],
		[CN_EML_PositedBy],
		[CN_EML_PositedAt],
		[CN_EML_PositReliability]
	)
	SELECT
		[p].[CN_EML_ID],
		COALESCE([i].[CN_EML_PositedBy], @PositedBy),
		COALESCE([i].[CN_EML_PositedAt], @Now),
		COALESCE([i].[CN_EML_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@EML_Posit		[p]
			ON	[p].[CN_EML_Checksum]	= [i].[CN_EML_Checksum];

END;
GO

CREATE TRIGGER [org].[CN_EML_Contact_EMail_Current_Update]
ON [org].[CN_EML_Contact_EMail_Current]
INSTEAD OF UPDATE
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(UPDATE([CN_EML_ID]))
		RAISERROR('Cannot update the identity column ''CN_EML_ID''.', 16, 1);
		--	THROW 50000, N'Cannot update the identity column ''CN_EML_ID''.', 1;

	IF(UPDATE([CN_EML_CN_ID]))
		RAISERROR('Cannot update the foreign key identity column ''CN_EML_ID''.', 16, 1);
		--	THROW 50000, N'Cannot update the foreign key identity column ''CN_EML_ID''.', 1;

	IF(UPDATE([CN_EML_Checksum]))
		RAISERROR('Cannot update the computed column ''CN_EML_Checksum''.', 16, 1);
		--	THROW 50000, N'Cannot update the computed column ''CN_EML_Checksum''.', 1;

	IF(UPDATE([CN_EML_PositReliable]))
		RAISERROR('Cannot update the computed column ''CN_EML_PositReliable''.', 16, 1);
		--	THROW 50000, N'Cannot update the computed column ''CN_EML_PositReliable''.', 1;

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @EML_Posit TABLE
	(
		[CN_EML_ID]					INT		NOT	NULL,
		[CN_EML_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([CN_EML_ID] ASC),
		UNIQUE CLUSTERED([CN_EML_Checksum] ASC)
	);

	INSERT INTO [org].[CN_EML_Contact_EMail_Posit]
	(
		[CN_EML_CN_ID],

		[CN_EML_Type_ID],
		[CN_EML_Address],

		[CN_EML_ChangedAt]
	)
	OUTPUT
		[inserted].[CN_EML_ID],
		[inserted].[CN_EML_Checksum]
	INTO
		@EML_Posit
		(
			[CN_EML_ID],
			[CN_EML_Checksum]
		)
	SELECT
		[i].[CN_EML_CN_ID],

		[i].[CN_EML_Type_ID],
		[i].[CN_EML_Address],

		COALESCE([i].[CN_EML_ChangedAt], @Now)
	FROM
		[inserted]		[i]
	WHERE	UPDATE([CN_EML_Address])
		OR	UPDATE([CN_EML_Type_ID]);

	INSERT INTO [org].[CN_EML_Contact_EMail_Annex]
	(
		[CN_EML_ID],
		[CN_EML_PositedBy],
		[CN_EML_PositedAt],
		[CN_EML_PositReliability]
	)
	SELECT
		[p].[CN_EML_ID],
		COALESCE([i].[CN_EML_PositedBy], @PositedBy),
		COALESCE([i].[CN_EML_PositedAt], @Now),
		COALESCE([i].[CN_EML_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@EML_Posit		[p]
			ON	[p].[CN_EML_Checksum]	= BINARY_CHECKSUM([i].[CN_EML_Address]);

	INSERT INTO [org].[CN_EML_Contact_EMail_Annex]
	(
		[CN_EML_ID],
		[CN_EML_PositedBy],
		[CN_EML_PositedAt],
		[CN_EML_PositReliability]
	)
	SELECT
		[i].[CN_EML_ID],
		COALESCE([i].[CN_EML_PositedBy], @PositedBy),
		COALESCE([i].[CN_EML_PositedAt], @Now),
		COALESCE([i].[CN_EML_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	WHERE	NOT(UPDATE([CN_EML_Address])
			OR	UPDATE([CN_EML_Type_ID]));

END;
GO

CREATE TRIGGER [org].[CN_EML_Contact_EMail_Current_Delete]
ON [org].[CN_EML_Contact_EMail_Current]
INSTEAD OF DELETE
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	INSERT INTO [org].[CN_EML_Contact_EMail_Annex]
	(
		[CN_EML_ID],
		[CN_EML_PositedBy],
		[CN_EML_PositedAt],
		[CN_EML_PositReliability]
	)
	SELECT
		[d].[CN_EML_ID],
		@PositedBy,
		@Now,
		0
	FROM
		[deleted]	[d]
	WHERE
		[d].[CN_EML_ID]	IS NOT NULL

END;
GO
