﻿CREATE FUNCTION [org].[CN_PHN_Contact_Phone_RW]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[p].[CN_PHN_ID],
	[p].[CN_PHN_CN_ID],

	[p].[CN_PHN_Type_ID],
	[p].[CN_PHN_Number],

	[p].[CN_PHN_Checksum],
	[p].[CN_PHN_ChangedAt],
	[a].[CN_PHN_PositedBy],
	[a].[CN_PHN_PositedAt],
	[a].[CN_PHN_PositReliability],
	[a].[CN_PHN_PositReliable]

FROM
	[org].[CN_PHN_Contact_Phone_Posit_RW](@ChangedBefore)				[p]

INNER JOIN
	[org].[CN_PHN_Contact_Phone_Annex_RW](@PositedBefore)				[a]
		ON	[a].[CN_PHN_ID]				= [p].[CN_PHN_ID]
		AND	[a].[CN_PHN_PositedAt]		= (
			SELECT TOP 1
				[s].[CN_PHN_PositedAt]
			FROM
				[org].[CN_PHN_Contact_Phone_Annex_RW](@PositedBefore)	[s]
			WHERE
				[s].[CN_PHN_ID]			= [a].[CN_PHN_ID]
			ORDER BY
				[s].[CN_PHN_PositedAt]	DESC
			);