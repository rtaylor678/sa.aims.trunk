﻿CREATE FUNCTION [org].[CN_EML_Contact_EMail_IsValid]
(
	--	Allow 2 extra characters in an e-mail for more accurate validation 
	@Address		NVARCHAR(256) 
)
RETURNS INT
AS
BEGIN

	SET @Address = RTRIM(LTRIM(@Address));

	DECLARE @AtLocation	SMALLINT = CHARINDEX('@', @Address);

	IF ((@Address <> '') AND (@AtLocation >= 1))
	BEGIN

		DECLARE @Local		VARCHAR(65)		= LEFT(@Address, @AtLocation - 1);
		DECLARE @Domain		VARCHAR(254)	= RIGHT(@Address, LEN(@Address) - @AtLocation);

		IF	(	(LEN(@Local)	<= 64)						--	Local length must be less than or equal to 64 characters
			AND	(LEN(@Domain)	<= 253)						--	Domain length must be less than or equal to 253 characters
			AND	(CHARINDEX('.', @Domain) >= 1)				--	Domain must include a "."
			AND (CHARINDEX('.', REVERSE(@Domain)) >= 3)		--	Top level domain must be at least 2 characters
			AND	(NOT @Address LIKE '%@%@%')					--	Only one "@" sign allowed
			AND	(NOT @Address LIKE '%@.%')					--	Domain must have a length
			AND	(NOT @Address LIKE '%..%')					--	No ".." sequence allowed
			)
		BEGIN
			RETURN 1;
		END;

	END;

	RETURN 0;

END;