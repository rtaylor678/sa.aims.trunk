﻿CREATE FUNCTION [org].[AS_NME_Assets_Name_RW]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[p].[AS_NME_ID],
	[p].[AS_NME_AS_ID],

	[p].[AS_NME_AssetTag],
	[p].[AS_NME_AssetName],
	[p].[AS_NME_AssetDetail],

	[p].[AS_NME_Checksum],
	[p].[AS_NME_ChangedAt],
	[a].[AS_NME_PositedBy],
	[a].[AS_NME_PositedAt],
	[a].[AS_NME_PositReliability],
	[a].[AS_NME_PositReliable]

FROM
	[org].[AS_NME_Assets_Name_Posit_RW](@ChangedBefore)				[p]

INNER JOIN
	[org].[AS_NME_Assets_Name_Annex_RW](@PositedBefore)				[a]
		ON	[a].[AS_NME_ID]				= [p].[AS_NME_ID]
		AND	[a].[AS_NME_PositedAt]		= (
			SELECT TOP 1
				[s].[AS_NME_PositedAt]
			FROM
				[org].[AS_NME_Assets_Name_Annex_RW](@PositedBefore)	[s]
			WHERE
				[s].[AS_NME_ID]			= [a].[AS_NME_ID]
			ORDER BY
				[s].[AS_NME_PositedAt]	DESC
			);