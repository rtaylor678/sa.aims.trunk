﻿CREATE FUNCTION [org].[CN_ADD_Contact_Address_RW]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[p].[CN_ADD_ID],
	[p].[CN_ADD_CN_ID],

	[p].[CN_ADD_Type_ID],
	[p].[CN_ADD_Country_ID],
	[p].[CN_ADD_State],
	[p].[CN_ADD_City],
	[p].[CN_ADD_PostalCode],
	[p].[CN_ADD_PoBox],
	[p].[CN_ADD_Address1],
	[p].[CN_ADD_Address2],
	[p].[CN_ADD_Address3],

	[p].[CN_ADD_Checksum],
	[p].[CN_ADD_ChangedAt],
	[a].[CN_ADD_PositedBy],
	[a].[CN_ADD_PositedAt],
	[a].[CN_ADD_PositReliability],
	[a].[CN_ADD_PositReliable]

FROM
	[org].[CN_ADD_Contact_Address_Posit_RW](@ChangedBefore)				[p]

INNER JOIN
	[org].[CN_ADD_Contact_Address_Annex_RW](@PositedBefore)				[a]
		ON	[a].[CN_ADD_ID]				= [p].[CN_ADD_ID]
		AND	[a].[CN_ADD_PositedAt]		= (
			SELECT TOP 1
				[s].[CN_ADD_PositedAt]
			FROM
				[org].[CN_ADD_Contact_Address_Annex_RW](@PositedBefore)	[s]
			WHERE
				[s].[CN_ADD_ID]			= [a].[CN_ADD_ID]
			ORDER BY
				[s].[CN_ADD_PositedAt]	DESC
			);