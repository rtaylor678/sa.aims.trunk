﻿CREATE FUNCTION [org].[PE_NME_People_Name_Rewind]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@Reliable		BIT					= 1
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[NME].[PE_NME_ID],
	[NME].[PE_NME_PE_ID],

	[NME].[PE_NME_NamePrefix],
	[NME].[PE_NME_NameFirst],
	[NME].[PE_NME_NameMiddle],
	[NME].[PE_NME_NameLast],
	[NME].[PE_NME_NameSuffix],
	[NME].[PE_NME_Title],

	[NME].[PE_NME_Checksum],
	[NME].[PE_NME_ChangedAt],
	[NME].[PE_NME_PositedBy],
	[NME].[PE_NME_PositedAt],
	[NME].[PE_NME_PositReliability],
	[NME].[PE_NME_PositReliable]

FROM
	[org].[PE_NME_People_Name_RW](@ChangedBefore, @PositedBefore)			[NME]
WHERE	[NME].[PE_NME_PositReliable]	= @Reliable
	AND	[NME].[PE_NME_ID]				= (
		SELECT TOP 1
			[NME_s].[PE_NME_ID]
		FROM
			[org].[PE_NME_People_Name_RW](@ChangedBefore, @PositedBefore)	[NME_s]
		WHERE	[NME_s].[PE_NME_PE_ID]	= [NME].[PE_NME_PE_ID]
		ORDER BY
			[NME_s].[PE_NME_ChangedAt]	DESC,
			[NME_s].[PE_NME_PositedAt]	DESC
		);