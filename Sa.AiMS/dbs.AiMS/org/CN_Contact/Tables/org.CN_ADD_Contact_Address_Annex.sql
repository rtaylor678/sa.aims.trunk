﻿CREATE TABLE [org].[CN_ADD_Contact_Address_Annex]
(
	[CN_ADD_ID]					INT					NOT	NULL	CONSTRAINT [FK__CN_ADD_Contact_Address_Annex_CN_ADD_ID]		REFERENCES [org].[CN_ADD_Contact_Address_Posit]([CN_ADD_ID]),

	[CN_ADD_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__CN_ADD_Contact_Address_Annex_PositedBy]		DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__CN_ADD_Contact_Address_Annex_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[CN_ADD_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__CN_ADD_Contact_Address_Annex_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[CN_ADD_PositReliability]	TINYINT				NOT	NULL	CONSTRAINT [DF__CN_ADD_Contact_Address_Annex_PositRel]		DEFAULT(50),
	[CN_ADD_PositReliable]		AS CONVERT(BIT, CASE WHEN [CN_ADD_PositReliability] > 0 THEN 1 ELSE 0 END)
								PERSISTED			NOT	NULL,
	[CN_ADD_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__CN_ADD_Contact_Address_Annex_RowGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__CN_ADD_Contact_Address_Annex_RowGuid]		UNIQUE NONCLUSTERED([CN_ADD_RowGuid]),
	CONSTRAINT [PK__CN_ADD_Contact_Address_Annex]	PRIMARY KEY CLUSTERED([CN_ADD_ID] ASC, [CN_ADD_PositedAt] DESC, [CN_ADD_PositedBy] ASC)
		WITH (FILLFACTOR = 95)
);