﻿CREATE VIEW [org].[CN_Contact_Current]
WITH SCHEMABINDING
AS
SELECT
	[CN].[CN_ID],
	[CN].[CN_PositedBy],
	[CN].[CN_PositedAt],

	[CN].[CN_ADD_ID],
	[CN].[CN_ADD_CN_ID],

	[CN].[CN_ADD_Type_ID],
	[CN].[CN_ADD_Country_ID],
	[CN].[CN_ADD_State],
	[CN].[CN_ADD_City],
	[CN].[CN_ADD_PostalCode],
	[CN].[CN_ADD_PoBox],
	[CN].[CN_ADD_Address1],
	[CN].[CN_ADD_Address2],
	[CN].[CN_ADD_Address3],

	[CN].[CN_ADD_Checksum],
	[CN].[CN_ADD_ChangedAt],
	[CN].[CN_ADD_PositedBy],
	[CN].[CN_ADD_PositedAt],
	[CN].[CN_ADD_PositReliability],
	[CN].[CN_ADD_PositReliable],

	[CN].[CN_EML_ID],
	[CN].[CN_EML_CN_ID],

	[CN].[CN_EML_Type_ID],
	[CN].[CN_EML_Address],

	[CN].[CN_EML_Checksum],
	[CN].[CN_EML_ChangedAt],
	[CN].[CN_EML_PositedBy],
	[CN].[CN_EML_PositedAt],
	[CN].[CN_EML_PositReliability],
	[CN].[CN_EML_PositReliable],

	[CN].[CN_PHN_ID],
	[CN].[CN_PHN_CN_ID],

	[CN].[CN_PHN_Type_ID],
	[CN].[CN_PHN_Number],

	[CN].[CN_PHN_Checksum],
	[CN].[CN_PHN_ChangedAt],
	[CN].[CN_PHN_PositedBy],
	[CN].[CN_PHN_PositedAt],
	[CN].[CN_PHN_PositReliability],
	[CN].[CN_PHN_PositReliable]

FROM [org].[CN_Contact_Rewind](DEFAULT, DEFAULT, DEFAULT) [CN];
GO

CREATE TRIGGER [org].[CN_Contact_Current_Insert]
ON [org].[CN_Contact_Current]
INSTEAD OF INSERT
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CN_ID] IS NOT NULL))
		RAISERROR('INSERT into the anchor identity column ''CN_ID'' is not allowed.', 16, 1);
		--	THROW 50000, N'INSERT into the anchor identity column ''CN_ID'' is not allowed.', 1;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CN_ADD_ID] IS NOT NULL))
		RAISERROR('INSERT into the identity column ''CN_ADD_ID'' is not allowed.', 16, 1) WITH NOWAIT;
		--	THROW 50000, N'INSERT into the identity column ''CN_ADD_ID'' is not allowed.', 1;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CN_ADD_CN_ID] IS NOT NULL))
		RAISERROR('INSERT into the foreign key identity column ''CN_ADD_CN_ID'' is not allowed.', 16, 1) WITH NOWAIT;
		--	THROW 50000, N'INSERT into the foreign key identity column ''CN_ADD_CN_ID'' is not allowed.', 1;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CN_ADD_Checksum] IS NOT NULL))
		RAISERROR('INSERT into the computed column ''CN_ADD_Checksum'' is not allowed.', 16, 1) WITH NOWAIT;
		--	THROW 50000, N'INSERT into the computed column ''CN_ADD_Checksum'' is not allowed.', 1;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CN_ADD_PositReliable] IS NOT NULL))
		RAISERROR('INSERT into the computed column ''CN_ADD_PositReliable'' is not allowed.', 16, 1) WITH NOWAIT;
		--	THROW 50000, N'INSERT into the computed column ''CN_ADD_PositReliable'' is not allowed.', 1;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CN_EML_ID] IS NOT NULL))
		RAISERROR('INSERT into the identity column ''CN_EML_ID'' is not allowed.', 16, 1) WITH NOWAIT;
		--	THROW 50000, N'INSERT into the identity column ''CN_EML_ID'' is not allowed.', 1;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CN_EML_CN_ID] IS NOT NULL))
		RAISERROR('INSERT into the foreign key identity column ''CN_EML_CN_ID'' is not allowed.', 16, 1) WITH NOWAIT;
		--	THROW 50000, N'INSERT into the foreign key identity column ''CN_EML_CN_ID'' is not allowed.', 1;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CN_EML_Checksum] IS NOT NULL))
		RAISERROR('INSERT into the computed column ''CN_EML_Checksum'' is not allowed.', 16, 1) WITH NOWAIT;
		--	THROW 50000, N'INSERT into the computed column ''CN_EML_Checksum'' is not allowed.', 1;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CN_EML_PositReliable] IS NOT NULL))
		RAISERROR('INSERT into the computed column ''CN_EML_PositReliable'' is not allowed.', 16, 1) WITH NOWAIT;
		--	THROW 50000, N'INSERT into the computed column ''CN_EML_PositReliable'' is not allowed.', 1;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CN_PHN_ID] IS NOT NULL))
		RAISERROR('INSERT into the identity column ''CN_PHN_ID'' is not allowed.', 16, 1) WITH NOWAIT;
		--	THROW 50000, N'INSERT into the identity column ''CN_PHN_ID'' is not allowed.', 1;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CN_PHN_CN_ID] IS NOT NULL))
		RAISERROR('INSERT into the foreign key identity column ''CN_PHN_CN_ID'' is not allowed.', 16, 1) WITH NOWAIT;
		--	THROW 50000, N'INSERT into the foreign key identity column ''CN_PHN_CN_ID'' is not allowed.', 1;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CN_PHN_Checksum] IS NOT NULL))
		RAISERROR('INSERT into the computed column ''CN_PHN_Checksum'' is not allowed.', 16, 1) WITH NOWAIT;
		--	THROW 50000, N'INSERT into the computed column ''CN_PHN_Checksum'' is not allowed.', 1;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CN_PHN_PositReliable] IS NOT NULL))
		RAISERROR('INSERT into the computed column ''CN_PHN_PositReliable'' is not allowed.', 16, 1) WITH NOWAIT;
		--	THROW 50000, N'INSERT into the computed column ''CN_PHN_PositReliable'' is not allowed.', 1;

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @Inserted TABLE
	(
		[ROW_ID]					INT					NOT	NULL,

		[CN_PositedBy]				INT					NOT	NULL,
		[CN_PositedAt]				DATETIMEOFFSET(7)	NOT	NULL,

		[CN_ADD_Type_ID]			INT					NOT	NULL,
		[CN_ADD_Country_ID]			INT						NULL,
		[CN_ADD_State]				NVARCHAR(24)			NULL	CHECK([CN_ADD_State] <> ''),
		[CN_ADD_City]				NVARCHAR(176)			NULL	CHECK([CN_ADD_City] <> ''),
		[CN_ADD_PostalCode]			NVARCHAR(24)			NULL	CHECK([CN_ADD_PostalCode] <> ''),
		[CN_ADD_PoBox]				NVARCHAR(16)			NULL	CHECK([CN_ADD_PoBox] <> ''),
		[CN_ADD_Address1]			NVARCHAR(256)			NULL	CHECK([CN_ADD_Address1] <> ''),
		[CN_ADD_Address2]			NVARCHAR(256)			NULL	CHECK([CN_ADD_Address2] <> ''),
		[CN_ADD_Address3]			NVARCHAR(256)			NULL	CHECK([CN_ADD_Address3] <> ''),
		[CN_ADD_Checksum]			AS BINARY_CHECKSUM([CN_ADD_Type_ID])
									PERSISTED			NOT	NULL,
		[CN_ADD_ChangedAt]			DATETIMEOFFSET(7)	NOT	NULL,
		[CN_ADD_PositedBy]			INT					NOT	NULL,
		[CN_ADD_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL,
		[CN_ADD_PositReliability]	TINYINT				NOT	NULL,

		[CN_EML_Type_ID]			INT						NULL,
		[CN_EML_Address]			NVARCHAR(254)			NULL	CHECK([CN_EML_Address] <> ''),
		[CN_EML_Checksum]			AS BINARY_CHECKSUM([CN_EML_Type_ID], [CN_EML_Address])
									PERSISTED			NOT	NULL,
		[CN_EML_ChangedAt]			DATETIMEOFFSET(7)	NOT	NULL,
		[CN_EML_PositedBy]			INT					NOT	NULL,
		[CN_EML_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL,
		[CN_EML_PositReliability]	TINYINT				NOT	NULL,

		[CN_PHN_Type_ID]			INT						NULL,
		[CN_PHN_Number]				VARCHAR(32)				NULL	CHECK([CN_PHN_Number] <> ''),
		[CN_PHN_Checksum]			AS BINARY_CHECKSUM([CN_PHN_Number])
									PERSISTED			NOT	NULL,
		[CN_PHN_ChangedAt]			DATETIMEOFFSET(7)	NOT	NULL,
		[CN_PHN_PositedBy]			INT					NOT	NULL,
		[CN_PHN_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL,
		[CN_PHN_PositReliability]	TINYINT				NOT	NULL,

		PRIMARY KEY([ROW_ID] ASC)
	)

	INSERT INTO @Inserted
	(
		[ROW_ID],

		[CN_PositedBy],
		[CN_PositedAt],

		[CN_ADD_Type_ID],
		[CN_ADD_Country_ID],
		[CN_ADD_State],
		[CN_ADD_City],
		[CN_ADD_PostalCode],
		[CN_ADD_PoBox],
		[CN_ADD_Address1],
		[CN_ADD_Address2],
		[CN_ADD_Address3],

		[CN_ADD_ChangedAt],
		[CN_ADD_PositedBy],
		[CN_ADD_PositedAt],
		[CN_ADD_PositReliability],

		[CN_EML_Type_ID],
		[CN_EML_Address],

		[CN_EML_ChangedAt],
		[CN_EML_PositedBy],
		[CN_EML_PositedAt],
		[CN_EML_PositReliability],

		[CN_PHN_Type_ID],
		[CN_PHN_Number],

		[CN_PHN_ChangedAt],
		[CN_PHN_PositedBy],
		[CN_PHN_PositedAt],
		[CN_PHN_PositReliability]

	)
	SELECT
		ROW_NUMBER() OVER (PARTITION BY [i].[CN_ID] ORDER BY [i].[CN_ID]),

		COALESCE([i].[CN_PositedBy], @PositedBy),
		COALESCE([i].[CN_PositedAt], @Now),

		[i].[CN_ADD_Type_ID],
		[i].[CN_ADD_Country_ID],
		[i].[CN_ADD_State],
		[i].[CN_ADD_City],
		[i].[CN_ADD_PostalCode],
		[i].[CN_ADD_PoBox],
		[i].[CN_ADD_Address1],
		[i].[CN_ADD_Address2],
		[i].[CN_ADD_Address3],

		COALESCE([i].[CN_ADD_ChangedAt], @Now),
		COALESCE([i].[CN_ADD_PositedBy], @PositedBy),
		COALESCE([i].[CN_ADD_PositedAt], @Now),
		COALESCE([i].[CN_ADD_PositReliability], @Reliability),

		[i].[CN_EML_Type_ID],
		[i].[CN_EML_Address],

		COALESCE([i].[CN_EML_ChangedAt], @Now),
		COALESCE([i].[CN_EML_PositedBy], @PositedBy),
		COALESCE([i].[CN_EML_PositedAt], @Now),
		COALESCE([i].[CN_EML_PositReliability], @Reliability),

		[i].[CN_PHN_Type_ID],
		[i].[CN_PHN_Number],

		COALESCE([i].[CN_PHN_ChangedAt], @Now),
		COALESCE([i].[CN_PHN_PositedBy], @PositedBy),
		COALESCE([i].[CN_PHN_PositedAt], @Now),
		COALESCE([i].[CN_PHN_PositReliability], @Reliability)

	FROM
		[inserted]	[i]
	LEFT OUTER JOIN
		[org].[CN_Contact_Current]	[c]
			ON	[c].[CN_EML_Checksum]	= BINARY_CHECKSUM([i].[CN_ADD_Type_ID], [i].[CN_ADD_Country_ID], [i].[CN_ADD_State], [i].[CN_ADD_City], [i].[CN_ADD_PostalCode], [i].[CN_ADD_PoBox], [i].[CN_ADD_Address1], [i].[CN_ADD_Address2], [i].[CN_ADD_Address3])
			AND	[c].[CN_EML_Checksum]	= BINARY_CHECKSUM([i].[CN_EML_Type_ID], [i].[CN_EML_Address])
			AND	[c].[CN_PHN_Checksum]	= BINARY_CHECKSUM([i].[CN_PHN_Type_ID], [i].[CN_PHN_Number])
	WHERE	[c].[CN_EML_CN_ID]	IS NULL;

	DECLARE @Anchor TABLE
	(
		[ROW_ID]					INT		NOT	NULL	IDENTITY(1, 1),
		[CN_ID]						INT		NOT	NULL,
		PRIMARY KEY([ROW_ID] ASC),
		UNIQUE CLUSTERED([CN_ID] ASC)
	);

	INSERT INTO [org].[CN_Contact]
	(
		[CN_PositedBy],
		[CN_PositedAt]
	)
	OUTPUT
		[inserted].[CN_ID]
	INTO
		@Anchor
		(
			[CN_ID]
		)
	SELECT
		[i].[CN_PositedBy],
		[i].[CN_PositedAt]
	FROM
		@Inserted	[i]
	ORDER BY
		[i].[ROW_ID] ASC;

	DECLARE @ADD_Posit TABLE
	(
		[CN_ADD_ID]					INT		NOT	NULL,
		[CN_ADD_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([CN_ADD_ID] ASC),
		UNIQUE CLUSTERED([CN_ADD_Checksum] ASC)
	);

	INSERT INTO [org].[CN_ADD_Contact_Address_Posit]
	(
		[CN_ADD_CN_ID],

		[CN_ADD_Type_ID],
		[CN_ADD_Country_ID],
		[CN_ADD_State],
		[CN_ADD_City],
		[CN_ADD_PostalCode],
		[CN_ADD_PoBox],
		[CN_ADD_Address1],
		[CN_ADD_Address2],
		[CN_ADD_Address3],

		[CN_ADD_ChangedAt]
	)
	OUTPUT
		[inserted].[CN_ADD_ID],
		[inserted].[CN_ADD_Checksum]
	INTO
		@ADD_Posit
		(
			[CN_ADD_ID],
			[CN_ADD_Checksum]
		)
	SELECT
		[a].[CN_ID],

		[i].[CN_ADD_Type_ID],
		[i].[CN_ADD_Country_ID],
		[i].[CN_ADD_State],
		[i].[CN_ADD_City],
		[i].[CN_ADD_PostalCode],
		[i].[CN_ADD_PoBox],
		[i].[CN_ADD_Address1],
		[i].[CN_ADD_Address2],
		[i].[CN_ADD_Address3],

		[i].[CN_ADD_ChangedAt]
	FROM
		@Inserted		[i]
	INNER JOIN
		@Anchor			[a]
			ON	[a].[ROW_ID]	= [i].[ROW_ID];

	INSERT INTO [org].[CN_ADD_Contact_Address_Annex]
	(
		[CN_ADD_ID],
		[CN_ADD_PositedBy],
		[CN_ADD_PositedAt],
		[CN_ADD_PositReliability]
	)
	SELECT
		[p].[CN_ADD_ID],
		[i].[CN_ADD_PositedBy],
		[i].[CN_ADD_PositedAt],
		[i].[CN_ADD_PositReliability]
	FROM
		@Inserted		[i]
	INNER JOIN
		@ADD_Posit		[p]
			ON	[p].[CN_ADD_Checksum]	= [i].[CN_ADD_Checksum];

	DECLARE @EML_Posit TABLE
	(
		[CN_EML_ID]					INT		NOT	NULL,
		[CN_EML_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([CN_EML_ID] ASC),
		UNIQUE CLUSTERED([CN_EML_Checksum] ASC)
	);

	INSERT INTO [org].[CN_EML_Contact_EMail_Posit]
	(
		[CN_EML_CN_ID],

		[CN_EML_Type_ID],
		[CN_EML_Address],

		[CN_EML_ChangedAt]
	)
	OUTPUT
		[inserted].[CN_EML_ID],
		[inserted].[CN_EML_Checksum]
	INTO
		@EML_Posit
		(
			[CN_EML_ID],
			[CN_EML_Checksum]
		)
	SELECT
		[a].[CN_ID],

		[i].[CN_EML_Type_ID],
		[i].[CN_EML_Address],

		[i].[CN_EML_ChangedAt]
	FROM
		@Inserted		[i]
	INNER JOIN
		@Anchor			[a]
			ON	[a].[ROW_ID]	= [i].[ROW_ID];

	INSERT INTO [org].[CN_EML_Contact_EMail_Annex]
	(
		[CN_EML_ID],
		[CN_EML_PositedBy],
		[CN_EML_PositedAt],
		[CN_EML_PositReliability]
	)
	SELECT
		[p].[CN_EML_ID],
		[i].[CN_EML_PositedBy],
		[i].[CN_EML_PositedAt],
		[i].[CN_EML_PositReliability]
	FROM
		@Inserted		[i]
	INNER JOIN
		@EML_Posit		[p]
			ON	[p].[CN_EML_Checksum]	= [i].[CN_EML_Checksum];

	DECLARE @PHN_Posit TABLE
	(
		[CN_PHN_ID]					INT		NOT	NULL,
		[CN_PHN_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([CN_PHN_ID] ASC),
		UNIQUE CLUSTERED([CN_PHN_Checksum] ASC)
	);

	INSERT INTO [org].[CN_PHN_Contact_Phone_Posit]
	(
		[CN_PHN_CN_ID],

		[CN_PHN_Type_ID],
		[CN_PHN_Number],

		[CN_PHN_ChangedAt]
	)
	OUTPUT
		[inserted].[CN_PHN_ID],
		[inserted].[CN_PHN_Checksum]
	INTO
		@PHN_Posit
		(
			[CN_PHN_ID],
			[CN_PHN_Checksum]
		)
	SELECT
		[a].[CN_ID],

		[i].[CN_PHN_Type_ID],
		[i].[CN_PHN_Number],

		[i].[CN_PHN_ChangedAt]
	FROM
		@Inserted		[i]
	INNER JOIN
		@Anchor			[a]
			ON	[a].[ROW_ID]	= [i].[ROW_ID];

	INSERT INTO [org].[CN_PHN_Contact_Phone_Annex]
	(
		[CN_PHN_ID],
		[CN_PHN_PositedBy],
		[CN_PHN_PositedAt],
		[CN_PHN_PositReliability]
	)
	SELECT
		[p].[CN_PHN_ID],
		[i].[CN_PHN_PositedBy],
		[i].[CN_PHN_PositedAt],
		[i].[CN_PHN_PositReliability]
	FROM
		@Inserted		[i]
	INNER JOIN
		@PHN_Posit		[p]
			ON	[p].[CN_PHN_Checksum]	= [i].[CN_PHN_Checksum];

END;
GO

CREATE TRIGGER [org].[CN_Contact_Current_Update]
ON [org].[CN_Contact_Current]
INSTEAD OF UPDATE
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(UPDATE([CN_ID]))
		RAISERROR('Cannot update the anchor identity column ''CN_ID''.', 16, 1);
		--	THROW 50000, N'Cannot update the anchor identity column ''CN_ID''.', 1;

	IF(UPDATE([CN_PositedBy]))
		RAISERROR('Cannot update the anchor column ''CN_PositedBy''.', 16, 1);
		--	THROW 50000, N'Cannot update the anchor column ''CN_PositedBy''.', 1;

	IF(UPDATE([CN_PositedAt]))
		RAISERROR('Cannot update the anchor column ''CN_PositedAt''.', 16, 1);
		--	THROW 50000, N'Cannot update the anchor column ''CN_PositedAt''.', 1;

	IF(UPDATE([CN_ADD_ID]))
		RAISERROR('Cannot update the identity column ''CN_ADD_ID''.', 16, 1);
		--	THROW 50000, N'Cannot update the identity column ''CN_ADD_ID''.', 1;

	IF(UPDATE([CN_ADD_CN_ID]))
		RAISERROR('Cannot update the foreign key identity column ''CN_ADD_CN_ID''.', 16, 1);
		--	THROW 50000, N'Cannot update the foreign key identity column ''CN_ADD_CN_ID''.', 1;

	IF(UPDATE([CN_ADD_Checksum]))
		RAISERROR('Cannot update the computed column ''CN_ADD_Checksum''.', 16, 1);
		--	THROW 50000, N'Cannot update the computed column ''CN_ADD_Checksum''.', 1;

	IF(UPDATE([CN_ADD_PositReliable]))
		RAISERROR('Cannot update the computed column ''CN_ADD_PositReliable''.', 16, 1);
		--	THROW 50000, N'Cannot update the computed column ''CN_ADD_PositReliable''.', 1;

	IF(UPDATE([CN_EML_ID]))
		RAISERROR('Cannot update the identity column ''CN_EML_ID''.', 16, 1);
		--	THROW 50000, N'Cannot update the identity column ''CN_EML_ID''.', 1;

	IF(UPDATE([CN_EML_CN_ID]))
		RAISERROR('Cannot update the foreign key identity column ''CN_EML_CN_ID''.', 16, 1);
		--	THROW 50000, N'Cannot update the foreign key identity column ''CN_EML_CN_ID''.', 1;

	IF(UPDATE([CN_EML_Checksum]))
		RAISERROR('Cannot update the computed column ''CN_EML_Checksum''.', 16, 1);
		--	THROW 50000, N'Cannot update the computed column ''CN_EML_Checksum''.', 1;

	IF(UPDATE([CN_EML_PositReliable]))
		RAISERROR('Cannot update the computed column ''CN_EML_PositReliable''.', 16, 1);
		--	THROW 50000, N'Cannot update the computed column ''CN_EML_PositReliable''.', 1;

	IF(UPDATE([CN_PHN_ID]))
		RAISERROR('Cannot update the identity column ''CN_PHN_ID''.', 16, 1);
		--	THROW 50000, N'Cannot update the identity column ''CN_PHN_ID''.', 1;

	IF(UPDATE([CN_PHN_CN_ID]))
		RAISERROR('Cannot update the foreign key identity column ''CN_PHN_CN_ID''.', 16, 1);
		--	THROW 50000, N'Cannot update the foreign key identity column ''CN_PHN_CN_ID''.', 1;

	IF(UPDATE([CN_PHN_Checksum]))
		RAISERROR('Cannot update the computed column ''CN_PHN_Checksum''.', 16, 1);
		--	THROW 50000, N'Cannot update the computed column ''CN_PHN_Checksum''.', 1;

	IF(UPDATE([CN_PHN_PositReliable]))
		RAISERROR('Cannot update the computed column ''CN_PHN_PositReliable''.', 16, 1);
		--	THROW 50000, N'Cannot update the computed column ''CN_PHN_PositReliable''.', 1;

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @ADD_Posit TABLE
	(
		[CN_ADD_ID]					INT		NOT	NULL,
		[CN_ADD_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([CN_ADD_ID] ASC),
		UNIQUE CLUSTERED([CN_ADD_Checksum] ASC)
	);

	INSERT INTO [org].[CN_ADD_Contact_Address_Posit]
	(
		[CN_ADD_CN_ID],

		[CN_ADD_Type_ID],
		[CN_ADD_Country_ID],
		[CN_ADD_State],
		[CN_ADD_City],
		[CN_ADD_PostalCode],
		[CN_ADD_PoBox],
		[CN_ADD_Address1],
		[CN_ADD_Address2],
		[CN_ADD_Address3],

		[CN_ADD_ChangedAt]
	)
	OUTPUT
		[inserted].[CN_ADD_ID],
		[inserted].[CN_ADD_Checksum]
	INTO
		@ADD_Posit
		(
			[CN_ADD_ID],
			[CN_ADD_Checksum]
		)
	SELECT
		[i].[CN_ADD_CN_ID],

		[i].[CN_ADD_Type_ID],
		[i].[CN_ADD_Country_ID],
		[i].[CN_ADD_State],
		[i].[CN_ADD_City],
		[i].[CN_ADD_PostalCode],
		[i].[CN_ADD_PoBox],
		[i].[CN_ADD_Address1],
		[i].[CN_ADD_Address2],
		[i].[CN_ADD_Address3],

		[i].[CN_ADD_ChangedAt] 
	FROM
		[inserted]		[i]
	WHERE	UPDATE([CN_ADD_Type_ID])
		OR	UPDATE([CN_ADD_Country_ID])
		OR	UPDATE([CN_ADD_State])
		OR	UPDATE([CN_ADD_City])
		OR	UPDATE([CN_ADD_PostalCode])
		OR	UPDATE([CN_ADD_PoBox])
		OR	UPDATE([CN_ADD_Address1])
		OR	UPDATE([CN_ADD_Address2])
		OR	UPDATE([CN_ADD_Address3]);

	INSERT INTO [org].[CN_ADD_Contact_Address_Annex]
	(
		[CN_ADD_ID],
		[CN_ADD_PositedBy],
		[CN_ADD_PositedAt],
		[CN_ADD_PositReliability]
	)
	SELECT
		[p].[CN_ADD_ID],
		COALESCE([i].[CN_ADD_PositedBy], @PositedBy),
		COALESCE([i].[CN_ADD_PositedAt], @Now),
		COALESCE([i].[CN_ADD_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@ADD_Posit		[p]
			ON	[p].[CN_ADD_Checksum]	= BINARY_CHECKSUM([i].[CN_ADD_Type_ID], [i].[CN_ADD_Country_ID], [i].[CN_ADD_State], [i].[CN_ADD_City], [i].[CN_ADD_PostalCode], [i].[CN_ADD_PoBox], [i].[CN_ADD_Address1], [i].[CN_ADD_Address2], [i].[CN_ADD_Address3]);

	INSERT INTO [org].[CN_ADD_Contact_Address_Annex]
	(
		[CN_ADD_ID],
		[CN_ADD_PositedBy],
		[CN_ADD_PositedAt],
		[CN_ADD_PositReliability]
	)
	SELECT
		[i].[CN_ADD_ID],
		COALESCE([i].[CN_ADD_PositedBy], @PositedBy),
		COALESCE([i].[CN_ADD_PositedAt], @Now),
		COALESCE([i].[CN_ADD_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	WHERE	[i].[CN_ADD_ID]	IS NOT NULL
		AND	NOT(UPDATE([CN_ADD_Type_ID])
			OR	UPDATE([CN_ADD_Country_ID])
			OR	UPDATE([CN_ADD_State])
			OR	UPDATE([CN_ADD_City])
			OR	UPDATE([CN_ADD_PostalCode])
			OR	UPDATE([CN_ADD_PoBox])
			OR	UPDATE([CN_ADD_Address1])
			OR	UPDATE([CN_ADD_Address2])
			OR	UPDATE([CN_ADD_Address3]));

	DECLARE @EML_Posit TABLE
	(
		[CN_EML_ID]					INT		NOT	NULL,
		[CN_EML_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([CN_EML_ID] ASC),
		UNIQUE CLUSTERED([CN_EML_Checksum] ASC)
	);

	INSERT INTO [org].[CN_EML_Contact_EMail_Posit]
	(
		[CN_EML_CN_ID],

		[CN_EML_Type_ID],
		[CN_EML_Address],

		[CN_EML_ChangedAt]
	)
	OUTPUT
		[inserted].[CN_EML_ID],
		[inserted].[CN_EML_Checksum]
	INTO
		@EML_Posit
		(
			[CN_EML_ID],
			[CN_EML_Checksum]
		)
	SELECT
		[i].[CN_EML_CN_ID],

		[i].[CN_EML_Type_ID],
		[i].[CN_EML_Address],

		[i].[CN_EML_ChangedAt] 
	FROM
		[inserted]		[i]
	WHERE	UPDATE([CN_EML_Type_ID])
		OR	UPDATE([CN_EML_Address]);

	INSERT INTO [org].[CN_EML_Contact_EMail_Annex]
	(
		[CN_EML_ID],
		[CN_EML_PositedBy],
		[CN_EML_PositedAt],
		[CN_EML_PositReliability]
	)
	SELECT
		[p].[CN_EML_ID],
		COALESCE([i].[CN_EML_PositedBy], @PositedBy),
		COALESCE([i].[CN_EML_PositedAt], @Now),
		COALESCE([i].[CN_EML_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@EML_Posit		[p]
			ON	[p].[CN_EML_Checksum]	= BINARY_CHECKSUM([i].[CN_EML_Type_ID], [i].[CN_EML_Address]);

	INSERT INTO [org].[CN_EML_Contact_EMail_Annex]
	(
		[CN_EML_ID],
		[CN_EML_PositedBy],
		[CN_EML_PositedAt],
		[CN_EML_PositReliability]
	)
	SELECT
		[i].[CN_EML_ID],
		COALESCE([i].[CN_EML_PositedBy], @PositedBy),
		COALESCE([i].[CN_EML_PositedAt], @Now),
		COALESCE([i].[CN_EML_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	WHERE	[i].[CN_EML_ID]	IS NOT NULL
		AND	NOT(UPDATE([CN_EML_Type_ID])
			OR	UPDATE([CN_EML_Address]));

	DECLARE @PHN_Posit TABLE
	(
		[CN_PHN_ID]					INT		NOT	NULL,
		[CN_PHN_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([CN_PHN_ID] ASC),
		UNIQUE CLUSTERED([CN_PHN_Checksum] ASC)
	);

	INSERT INTO [org].[CN_PHN_Contact_Phone_Posit]
	(
		[CN_PHN_CN_ID],

		[CN_PHN_Type_ID],
		[CN_PHN_Number],

		[CN_PHN_ChangedAt]
	)
	OUTPUT
		[inserted].[CN_PHN_ID],
		[inserted].[CN_PHN_Checksum]
	INTO
		@PHN_Posit
		(
			[CN_PHN_ID],
			[CN_PHN_Checksum]
		)
	SELECT
		[i].[CN_PHN_CN_ID],

		[i].[CN_PHN_Type_ID],
		[i].[CN_PHN_Number],

		[i].[CN_PHN_ChangedAt] 
	FROM
		[inserted]		[i]
	WHERE	UPDATE([CN_PHN_Type_ID])
		OR	UPDATE([CN_PHN_Number]);

	INSERT INTO [org].[CN_PHN_Contact_Phone_Annex]
	(
		[CN_PHN_ID],
		[CN_PHN_PositedBy],
		[CN_PHN_PositedAt],
		[CN_PHN_PositReliability]
	)
	SELECT
		[p].[CN_PHN_ID],
		COALESCE([i].[CN_PHN_PositedBy], @PositedBy),
		COALESCE([i].[CN_PHN_PositedAt], @Now),
		COALESCE([i].[CN_PHN_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@PHN_Posit		[p]
			ON	[p].[CN_PHN_Checksum]	= BINARY_CHECKSUM([i].[CN_PHN_Type_ID], [i].[CN_PHN_Number]);

	INSERT INTO [org].[CN_PHN_Contact_Phone_Annex]
	(
		[CN_PHN_ID],
		[CN_PHN_PositedBy],
		[CN_PHN_PositedAt],
		[CN_PHN_PositReliability]
	)
	SELECT
		[i].[CN_PHN_ID],
		COALESCE([i].[CN_PHN_PositedBy], @PositedBy),
		COALESCE([i].[CN_PHN_PositedAt], @Now),
		COALESCE([i].[CN_PHN_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	WHERE	[i].[CN_PHN_ID]	IS NOT NULL
		AND	NOT(UPDATE([CN_PHN_Type_ID])
			OR	UPDATE([CN_PHN_Number]));

END;
GO

CREATE TRIGGER [org].[CN_Contact_Current_Delete]
ON [org].[CN_Contact_Current]
INSTEAD OF DELETE
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	INSERT INTO [org].[CN_ADD_Contact_Address_Annex]
	(
		[CN_ADD_ID],
		[CN_ADD_PositedBy],
		[CN_ADD_PositedAt],
		[CN_ADD_PositReliability]
	)
	SELECT
		[d].[CN_ADD_ID],
		@PositedBy,
		@Now,
		0
	FROM
		[deleted]	[d]
	WHERE
		[d].[CN_ADD_ID]	IS NOT NULL;

	INSERT INTO [org].[CN_EML_Contact_EMail_Annex]
	(
		[CN_EML_ID],
		[CN_EML_PositedBy],
		[CN_EML_PositedAt],
		[CN_EML_PositReliability]
	)
	SELECT
		[d].[CN_EML_ID],
		@PositedBy,
		@Now,
		0
	FROM
		[deleted]	[d]
	WHERE
		[d].[CN_EML_ID]	IS NOT NULL;

	INSERT INTO [org].[CN_PHN_Contact_Phone_Annex]
	(
		[CN_PHN_ID],
		[CN_PHN_PositedBy],
		[CN_PHN_PositedAt],
		[CN_PHN_PositReliability]
	)
	SELECT
		[d].[CN_PHN_ID],
		@PositedBy,
		@Now,
		0
	FROM
		[deleted]	[d]
	WHERE
		[d].[CN_PHN_ID]	IS NOT NULL;

END;
GO