﻿CREATE FUNCTION [org].[CN_PHN_Contact_Phone_Rewind]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@Reliable		BIT					= 1
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[PHN].[CN_PHN_ID],
	[PHN].[CN_PHN_CN_ID],

	[PHN].[CN_PHN_Type_ID],
	[PHN].[CN_PHN_Number],

	[PHN].[CN_PHN_Checksum],
	[PHN].[CN_PHN_ChangedAt],
	[PHN].[CN_PHN_PositedBy],
	[PHN].[CN_PHN_PositedAt],
	[PHN].[CN_PHN_PositReliability],
	[PHN].[CN_PHN_PositReliable]

FROM
	[org].[CN_PHN_Contact_Phone_RW](@ChangedBefore, @PositedBefore)			[PHN]

WHERE	[PHN].[CN_PHN_PositReliable]	= @Reliable
	AND	[PHN].[CN_PHN_ID]				= (
		SELECT TOP 1
			[PHN_s].[CN_PHN_ID]
		FROM
			[org].[CN_PHN_Contact_Phone_RW](@ChangedBefore, @PositedBefore)	[PHN_s]
		WHERE
			[PHN_s].[CN_PHN_CN_ID]		= [PHN].[CN_PHN_CN_ID]
		ORDER BY
			[PHN_s].[CN_PHN_ChangedAt]	DESC,
			[PHN_s].[CN_PHN_PositedAt]	DESC
		);