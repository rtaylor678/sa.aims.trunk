﻿CREATE TABLE [org].[AS_GEO_Assets_Location_Annex]
(
	[AS_GEO_ID]					INT					NOT	NULL	CONSTRAINT [FK__AS_GEO_Assets_Location_Annex_AS_GEO_ID]	REFERENCES [org].[AS_GEO_Assets_Location_Posit]([AS_GEO_ID]),

	[AS_GEO_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__AS_GEO_Assets_Location_Annex_PositedBy]	DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__AS_GEO_Assets_Location_Annex_PositedBy]	REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[AS_GEO_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__AS_GEO_Assets_Location_Annex_PositedAt]	DEFAULT(SYSDATETIMEOFFSET()),
	[AS_GEO_PositReliability]	TINYINT				NOT	NULL	CONSTRAINT [DF__AS_GEO_Assets_Location_Annex_PositRel]	DEFAULT(50),
	[AS_GEO_PositReliable]		AS CONVERT(BIT, CASE WHEN [AS_GEO_PositReliability] > 0 THEN 1 ELSE 0 END)
								PERSISTED			NOT	NULL,
	[AS_GEO_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__AS_GEO_Assets_Location_Annex_RowGuid]	DEFAULT(NEWSEQUENTIALID()) ROWGUIDCOL,
																CONSTRAINT [UX__AS_GEO_Assets_Location_Annex_RowGuid]	UNIQUE NONCLUSTERED([AS_GEO_RowGuid]),
	CONSTRAINT [PK__AS_GEO_Assets_Location_Annex]	PRIMARY KEY CLUSTERED([AS_GEO_ID] ASC, [AS_GEO_PositedAt] DESC, [AS_GEO_PositedBy] ASC)
		WITH (FILLFACTOR = 95)
);