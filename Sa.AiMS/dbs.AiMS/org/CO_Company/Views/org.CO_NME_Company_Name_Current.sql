﻿CREATE VIEW [org].[CO_NME_Company_Name_Current]
WITH SCHEMABINDING
AS
SELECT
	[NME].[CO_NME_ID],
	[NME].[CO_NME_CO_ID],

	[NME].[CO_NME_CompanyTag],
	[NME].[CO_NME_CompanyName],
	[NME].[CO_NME_CompanyDetail],

	[NME].[CO_NME_Checksum],
	[NME].[CO_NME_ChangedAt],
	[NME].[CO_NME_PositedBy],
	[NME].[CO_NME_PositedAt],
	[NME].[CO_NME_PositReliability],
	[NME].[CO_NME_PositReliable]

FROM [org].[CO_NME_Company_Name_Rewind](DEFAULT, DEFAULT, DEFAULT)	[NME];
GO

CREATE TRIGGER [org].[CO_NME_Company_Name_Current_Insert]
ON [org].[CO_NME_Company_Name_Current]
INSTEAD OF INSERT
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CO_NME_ID] IS NOT NULL))
		RAISERROR('INSERT into the identity column ''CO_NME_ID'' is not allowed.', 16, 1) WITH NOWAIT;
		--	THROW 50000, N'INSERT into the identity column ''CO_NME_ID'' is not allowed.', 1;

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @NME_Posit TABLE
	(
		[CO_NME_ID]					INT		NOT	NULL,
		[CO_NME_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([CO_NME_ID] ASC),
		UNIQUE CLUSTERED([CO_NME_Checksum] ASC)
	);

	INSERT INTO [org].[CO_NME_Company_Name_Posit]
	(
		[CO_NME_CO_ID],

		[CO_NME_CompanyTag],
		[CO_NME_CompanyName],
		[CO_NME_CompanyDetail],

		[CO_NME_ChangedAt]
	)
	OUTPUT
		[inserted].[CO_NME_ID],
		[inserted].[CO_NME_Checksum]
	INTO
		@NME_Posit
		(
			[CO_NME_ID],
			[CO_NME_Checksum]
		)
	SELECT
		[i].[CO_NME_CO_ID],

		[i].[CO_NME_CompanyTag],
		[i].[CO_NME_CompanyName],
		[i].[CO_NME_CompanyDetail],

		COALESCE([i].[CO_NME_ChangedAt], @Now)
	FROM
		[inserted]		[i];

	INSERT INTO [org].[CO_NME_Company_Name_Annex]
	(
		[CO_NME_ID],
		[CO_NME_PositedBy],
		[CO_NME_PositedAt],
		[CO_NME_PositReliability]
	)
	SELECT
		[p].[CO_NME_ID],
		COALESCE([i].[CO_NME_PositedBy], @PositedBy),
		COALESCE([i].[CO_NME_PositedAt], @Now),
		COALESCE([i].[CO_NME_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@NME_Posit		[p]
			ON	[p].[CO_NME_Checksum]	= [i].[CO_NME_Checksum];

END;
GO

CREATE TRIGGER [org].[CO_NME_Company_Name_Current_Update]
ON [org].[CO_NME_Company_Name_Current]
INSTEAD OF UPDATE
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(UPDATE([CO_NME_ID]))
		RAISERROR('Cannot update the identity column ''CO_NME_ID''.', 16, 1);
		--	THROW 50000, N'Cannot update the identity column ''CO_NME_ID''.', 1;

	IF(UPDATE([CO_NME_CO_ID]))
		RAISERROR('Cannot update the foreign key identity column ''CO_NME_CO_ID''.', 16, 1);
		--	THROW 50000, N'Cannot update the foreign key identity column ''CO_NME_CO_ID''.', 1;

	IF(UPDATE([CO_NME_Checksum]))
		RAISERROR('Cannot update the computed column ''CO_NME_Checksum''.', 16, 1);
		--	THROW 50000, N'Cannot update the computed column ''CO_NME_Checksum''.', 1;

	IF(UPDATE([CO_NME_PositReliable]))
		RAISERROR('Cannot update the computed column ''CO_NME_PositReliable''.', 16, 1);
		--	THROW 50000, N'Cannot update the computed column ''CO_NME_PositReliable''.', 1;

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @NME_Posit TABLE
	(
		[CO_NME_ID]					INT		NOT	NULL,
		[CO_NME_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([CO_NME_ID] ASC),
		UNIQUE CLUSTERED([CO_NME_Checksum] ASC)
	);

	INSERT INTO [org].[CO_NME_Company_Name_Posit]
	(
		[CO_NME_CO_ID],

		[CO_NME_CompanyTag],
		[CO_NME_CompanyName],
		[CO_NME_CompanyDetail],

		[CO_NME_ChangedAt]
	)
	OUTPUT
		[inserted].[CO_NME_ID],
		[inserted].[CO_NME_Checksum]
	INTO
		@NME_Posit
		(
			[CO_NME_ID],
			[CO_NME_Checksum]
		)
	SELECT
		[i].[CO_NME_CO_ID],

		[i].[CO_NME_CompanyTag],
		[i].[CO_NME_CompanyName],
		[i].[CO_NME_CompanyDetail],

		COALESCE([i].[CO_NME_ChangedAt], @Now)
	FROM
		[inserted]		[i]
	WHERE	UPDATE([CO_NME_CompanyTag])
		OR	UPDATE([CO_NME_CompanyName])
		OR	UPDATE([CO_NME_CompanyDetail]);

	INSERT INTO [org].[CO_NME_Company_Name_Annex]
	(
		[CO_NME_ID],
		[CO_NME_PositedBy],
		[CO_NME_PositedAt],
		[CO_NME_PositReliability]
	)
	SELECT
		[p].[CO_NME_ID],
		COALESCE([i].[CO_NME_PositedBy], @PositedBy),
		COALESCE([i].[CO_NME_PositedAt], @Now),
		COALESCE([i].[CO_NME_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@NME_Posit		[p]
			ON	[p].[CO_NME_Checksum]	= BINARY_CHECKSUM([i].[CO_NME_CompanyTag]);

	INSERT INTO [org].[CO_NME_Company_Name_Annex]
	(
		[CO_NME_ID],
		[CO_NME_PositedBy],
		[CO_NME_PositedAt],
		[CO_NME_PositReliability]
	)
	SELECT
		[i].[CO_NME_ID],
		COALESCE([i].[CO_NME_PositedBy], @PositedBy),
		COALESCE([i].[CO_NME_PositedAt], @Now),
		COALESCE([i].[CO_NME_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	WHERE	NOT (UPDATE([CO_NME_CompanyTag])
		OR	UPDATE([CO_NME_CompanyName])
		OR	UPDATE([CO_NME_CompanyDetail]));

END;
GO

CREATE TRIGGER [org].[CO_NME_Company_Name_Current_Delete]
ON [org].[CO_NME_Company_Name_Current]
INSTEAD OF DELETE
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	INSERT INTO [org].[CO_NME_Company_Name_Annex]
	(
		[CO_NME_ID],
		[CO_NME_PositedBy],
		[CO_NME_PositedAt],
		[CO_NME_PositReliability]
	)
	SELECT
		[d].[CO_NME_ID],
		@PositedBy,
		@Now,
		0
	FROM
		[deleted]	[d]
	WHERE
		[d].[CO_NME_ID]	IS NOT NULL

END;
GO