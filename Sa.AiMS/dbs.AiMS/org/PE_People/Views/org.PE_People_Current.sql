﻿CREATE VIEW [org].[PE_People_Current]
WITH SCHEMABINDING
AS
SELECT
	[PE].[PE_ID],
	[PE].[PE_PositedBy],
	[PE].[PE_PositedAt],

	[PE].[PE_NME_ID],
	[PE].[PE_NME_PE_ID],

	[PE].[PE_NME_NamePrefix],
	[PE].[PE_NME_NameFirst],
	[PE].[PE_NME_NameMiddle],
	[PE].[PE_NME_NameLast],
	[PE].[PE_NME_NameSuffix],
	[PE].[PE_NME_Title],

	[PE].[PE_NME_Checksum],
	[PE].[PE_NME_ChangedAt],
	[PE].[PE_NME_PositedBy],
	[PE].[PE_NME_PositedAt],
	[PE].[PE_NME_PositReliability],
	[PE].[PE_NME_PositReliable]

FROM [org].[PE_People_Rewind](DEFAULT, DEFAULT, DEFAULT)	[PE];
GO

CREATE TRIGGER [org].[PE_People_Current_Insert]
ON [org].[PE_People_Current]
INSTEAD OF INSERT
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[PE_ID] IS NOT NULL))
		RAISERROR('INSERT into the anchor identity column ''PE_ID'' is not allowed.', 16, 1) WITH NOWAIT;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[PE_NME_ID] IS NOT NULL))
		RAISERROR('INSERT into the identity column ''PE_NME_ID'' is not allowed.', 16, 1) WITH NOWAIT;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[PE_NME_PE_ID] IS NOT NULL))
		RAISERROR('INSERT into the foreign key identity column ''PE_NME_PE_ID'' is not allowed.', 16, 1) WITH NOWAIT;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[PE_NME_Checksum] IS NOT NULL))
		RAISERROR('INSERT into the computed column ''PE_NME_Checksum'' is not allowed.', 16, 1) WITH NOWAIT;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[PE_NME_PositReliable] IS NOT NULL))
		RAISERROR('INSERT into the computed column ''PE_NME_PositReliable'' is not allowed.', 16, 1) WITH NOWAIT;

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @Inserted TABLE
	(
		[ROW_ID]					INT					NOT	NULL,

		[PE_PositedBy]				INT					NOT	NULL,
		[PE_PositedAt]				DATETIMEOFFSET(7)	NOT	NULL,

		[PE_NME_NamePrefix]			NVARCHAR(8)				NULL	CHECK([PE_NME_NamePrefix] <> ''),
		[PE_NME_NameFirst]			NVARCHAR(24)		NOT	NULL	CHECK([PE_NME_NameFirst] <> ''),
		[PE_NME_NameMiddle]			NVARCHAR(24)			NULL	CHECK([PE_NME_NameMiddle] <> ''),
		[PE_NME_NameLast]			NVARCHAR(48)		NOT	NULL	CHECK([PE_NME_NameLast] <> ''),
		[PE_NME_NameSuffix]			NVARCHAR(8)				NULL	CHECK([PE_NME_NameSuffix] <> ''),
		[PE_NME_Title]				NVARCHAR(72)			NULL	CHECK([PE_NME_Title] <> ''),
		[PE_NME_Checksum]			AS BINARY_CHECKSUM([PE_NME_NamePrefix], [PE_NME_NameFirst], [PE_NME_NameMiddle], [PE_NME_NameLast], [PE_NME_NameSuffix], [PE_NME_Title])
									PERSISTED			NOT	NULL,
		[PE_NME_ChangedAt]			DATETIMEOFFSET(7)	NOT	NULL,
		[PE_NME_PositedBy]			INT					NOT	NULL,
		[PE_NME_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL,
		[PE_NME_PositReliability]	TINYINT				NOT	NULL,

		PRIMARY KEY([ROW_ID] ASC)
	)

	INSERT INTO @Inserted
	(
		[ROW_ID],

		[PE_PositedBy],
		[PE_PositedAt],

		[PE_NME_NamePrefix],
		[PE_NME_NameFirst],
		[PE_NME_NameMiddle],
		[PE_NME_NameLast],
		[PE_NME_NameSuffix],
		[PE_NME_Title],
		[PE_NME_ChangedAt],
		[PE_NME_PositedBy],
		[PE_NME_PositedAt],
		[PE_NME_PositReliability]
	)
	SELECT
		ROW_NUMBER() OVER (PARTITION BY [i].[PE_ID] ORDER BY [i].[PE_ID]),

		COALESCE([i].[PE_PositedBy], @PositedBy),
		COALESCE([i].[PE_PositedAt], @Now),

		[i].[PE_NME_NamePrefix],
		[i].[PE_NME_NameFirst],
		[i].[PE_NME_NameMiddle],
		[i].[PE_NME_NameLast],
		[i].[PE_NME_NameSuffix],
		[i].[PE_NME_Title],
		COALESCE([i].[PE_NME_ChangedAt], @Now),
		COALESCE([i].[PE_NME_PositedBy], @PositedBy),
		COALESCE([i].[PE_NME_PositedAt], @Now),
		COALESCE([i].[PE_NME_PositReliability], @Reliability)

	FROM
		[inserted]	[i]
	LEFT OUTER JOIN
		[org].[PE_People_Current]	[c]
			ON	[c].[PE_NME_Checksum]	= BINARY_CHECKSUM([i].[PE_NME_NamePrefix], [i].[PE_NME_NameFirst], [i].[PE_NME_NameMiddle], [i].[PE_NME_NameLast], [i].[PE_NME_NameSuffix], [i].[PE_NME_Title])
	WHERE	[c].[PE_NME_PE_ID]	IS NULL;

	DECLARE @Anchor TABLE
	(
		[ROW_ID]					INT		NOT	NULL	IDENTITY(1, 1),
		[PE_ID]						INT		NOT	NULL,
		PRIMARY KEY([ROW_ID] ASC),
		UNIQUE CLUSTERED([PE_ID] ASC)
	);

	INSERT INTO [org].[PE_People]
	(
		[PE_PositedBy],
		[PE_PositedAt]
	)
	OUTPUT
		[inserted].[PE_ID]
	INTO
		@Anchor
		(
			[PE_ID]
		)
	SELECT
		[i].[PE_PositedBy],
		[i].[PE_PositedAt]
	FROM
		@Inserted	[i];

	DECLARE @NME_Posit TABLE
	(
		[PE_NME_ID]					INT		NOT	NULL,
		[PE_NME_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([PE_NME_ID] ASC),
		UNIQUE CLUSTERED([PE_NME_Checksum] ASC)
	);

	INSERT INTO [org].[PE_NME_People_Name_Posit]
	(
		[PE_NME_PE_ID],
		[PE_NME_NamePrefix],
		[PE_NME_NameFirst],
		[PE_NME_NameMiddle],
		[PE_NME_NameLast],
		[PE_NME_NameSuffix],
		[PE_NME_Title],
		[PE_NME_ChangedAt]
	)
	OUTPUT
		[inserted].[PE_NME_ID],
		[inserted].[PE_NME_Checksum]
	INTO
		@NME_Posit
		(
			[PE_NME_ID],
			[PE_NME_Checksum]
		)
	SELECT
		[a].[PE_ID],
		[i].[PE_NME_NamePrefix],
		[i].[PE_NME_NameFirst],
		[i].[PE_NME_NameMiddle],
		[i].[PE_NME_NameLast],
		[i].[PE_NME_NameSuffix],
		[i].[PE_NME_Title],
		[i].[PE_NME_ChangedAt] 
	FROM
		@Inserted		[i]
	INNER JOIN
		@Anchor			[a]
			ON	[a].[ROW_ID]	= [i].[ROW_ID];

	INSERT INTO [org].[PE_NME_People_Name_Annex]
	(
		[PE_NME_ID],
		[PE_NME_PositedBy],
		[PE_NME_PositedAt],
		[PE_NME_PositReliability]
	)
	SELECT
		[p].[PE_NME_ID],
		[i].[PE_NME_PositedBy],
		[i].[PE_NME_PositedAt],
		[i].[PE_NME_PositReliability]
	FROM
		@Inserted		[i]
	INNER JOIN
		@NME_Posit		[p]
			ON	[p].[PE_NME_Checksum]	= [i].[PE_NME_Checksum];

END;
GO

CREATE TRIGGER [org].[PE_People_Current_Update]
ON [org].[PE_People_Current]
INSTEAD OF UPDATE
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(UPDATE([PE_ID]))
		RAISERROR('Cannot update the anchor identity column ''PE_ID''.', 16, 1);

	IF(UPDATE([PE_PositedBy]))
		RAISERROR('Cannot update the anchor column ''PE_PositedBy''.', 16, 1);

	IF(UPDATE([PE_PositedAt]))
		RAISERROR('Cannot update the anchor column ''PE_PositedAt''.', 16, 1);

	IF(UPDATE([PE_NME_ID]))
		RAISERROR('Cannot update the identity column ''PE_NME_ID''.', 16, 1);

	IF(UPDATE([PE_NME_PE_ID]))
		RAISERROR('Cannot update the foreign key identity column ''PE_NME_PE_ID''.', 16, 1);

	IF(UPDATE([PE_NME_Checksum]))
		RAISERROR('Cannot update the computed column ''PE_NME_Checksum''.', 16, 1);

	IF(UPDATE([PE_NME_PositReliable]))
		RAISERROR('Cannot update the computed column ''PE_NME_PositReliable''.', 16, 1);

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @NME_Posit TABLE
	(
		[PE_NME_ID]					INT		NOT	NULL,
		[PE_NME_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([PE_NME_ID] ASC),
		UNIQUE CLUSTERED([PE_NME_Checksum] ASC)
	);

	INSERT INTO [org].[PE_NME_People_Name_Posit]
	(
		[PE_NME_PE_ID],
		[PE_NME_NamePrefix],
		[PE_NME_NameFirst],
		[PE_NME_NameMiddle],
		[PE_NME_NameLast],
		[PE_NME_NameSuffix],
		[PE_NME_Title],
		[PE_NME_ChangedAt]
	)
	OUTPUT
		[inserted].[PE_NME_ID],
		[inserted].[PE_NME_Checksum]
	INTO
		@NME_Posit
		(
			[PE_NME_ID],
			[PE_NME_Checksum]
		)
	SELECT
		[i].[PE_NME_PE_ID],
		[i].[PE_NME_NamePrefix],
		[i].[PE_NME_NameFirst],
		[i].[PE_NME_NameMiddle],
		[i].[PE_NME_NameLast],
		[i].[PE_NME_NameSuffix],
		[i].[PE_NME_Title],
		COALESCE([i].[PE_NME_ChangedAt], @Now)
	FROM
		[inserted]		[i]
	WHERE	UPDATE([PE_NME_NamePrefix])
		OR	UPDATE([PE_NME_NameFirst])
		OR	UPDATE([PE_NME_NameMiddle])
		OR	UPDATE([PE_NME_NameLast])
		OR	UPDATE([PE_NME_NameSuffix])
		OR	UPDATE([PE_NME_Title]);

	INSERT INTO [org].[PE_NME_People_Name_Annex]
	(
		[PE_NME_ID],
		[PE_NME_PositedBy],
		[PE_NME_PositedAt],
		[PE_NME_PositReliability]
	)
	SELECT
		[p].[PE_NME_ID],
		COALESCE([i].[PE_NME_PositedBy], @PositedBy),
		COALESCE([i].[PE_NME_PositedAt], @Now),
		COALESCE([i].[PE_NME_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@NME_Posit		[p]
			ON	[p].[PE_NME_Checksum]	= BINARY_CHECKSUM([i].[PE_NME_NamePrefix], [i].[PE_NME_NameFirst], [i].[PE_NME_NameMiddle], [i].[PE_NME_NameLast], [i].[PE_NME_NameSuffix], [i].[PE_NME_Title])

	INSERT INTO [org].[PE_NME_People_Name_Annex]
	(
		[PE_NME_ID],
		[PE_NME_PositedBy],
		[PE_NME_PositedAt],
		[PE_NME_PositReliability]
	)
	SELECT
		[i].[PE_NME_ID],
		COALESCE([i].[PE_NME_PositedBy], @PositedBy),
		COALESCE([i].[PE_NME_PositedAt], @Now),
		COALESCE([i].[PE_NME_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	WHERE	[i].[PE_NME_ID]	IS NOT NULL
		AND	NOT(UPDATE([PE_NME_NamePrefix])
			OR	UPDATE([PE_NME_NameFirst])
			OR	UPDATE([PE_NME_NameMiddle])
			OR	UPDATE([PE_NME_NameLast])
			OR	UPDATE([PE_NME_NameSuffix])
			OR	UPDATE([PE_NME_Title]));

END;
GO

CREATE TRIGGER [org].[PE_People_Current_Delete]
ON [org].[PE_People_Current]
INSTEAD OF DELETE
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	INSERT INTO [org].[PE_NME_People_Name_Annex]
	(
		[PE_NME_ID],
		[PE_NME_PositedBy],
		[PE_NME_PositedAt],
		[PE_NME_PositReliability]
	)
	SELECT
		[d].[PE_NME_ID],
		@PositedBy,
		@Now,
		0
	FROM
		[deleted]	[d]
	WHERE
		[d].[PE_NME_ID]	IS NOT NULL;

END;
GO