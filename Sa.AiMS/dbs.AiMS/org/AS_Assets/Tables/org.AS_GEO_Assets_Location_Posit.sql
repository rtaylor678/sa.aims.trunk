﻿CREATE TABLE [org].[AS_GEO_Assets_Location_Posit]
(
	[AS_GEO_ID]					INT					NOT	NULL	IDENTITY(1, 1),
	[AS_GEO_AS_ID]				INT					NOT	NULL	CONSTRAINT [FK__AS_GEO_Assets_Location_Posit_AS_GEO_AS_ID]		REFERENCES [org].[AS_Assets]([AS_ID]),

    [AS_GEO_Longitude_Deg]		DECIMAL(17, 14)		NOT	NULL	CONSTRAINT [CR__AS_GEO_Assets_Location_Posit_Longitude_Deg_L]	CHECK([AS_GEO_Longitude_Deg] >= -180.0),
																CONSTRAINT [CR__AS_GEO_Assets_Location_Posit_Longitude_Deg_U]	CHECK([AS_GEO_Longitude_Deg] <=  180.0),
    [AS_GEO_Latitude_Deg]		DECIMAL(17, 14)		NOT	NULL	CONSTRAINT [CR__AS_GEO_Assets_Location_Posit_Latitude_Deg_L]	CHECK([AS_GEO_Latitude_Deg] >= -90.0),
																CONSTRAINT [CR__AS_GEO_Assets_Location_Posit_Latitude_Deg_U]	CHECK([AS_GEO_Latitude_Deg] <=  90.0),
	[AS_GEO_Wgs84]				AS geography::STPointFromText('POINT(' + CONVERT(VARCHAR(19), [AS_GEO_Longitude_Deg], 0) + ' ' + CONVERT(VARCHAR(19), [AS_GEO_Latitude_Deg], 0) + ')', 4326),
	[AS_GEO_Checksum]			AS BINARY_CHECKSUM([AS_GEO_Longitude_Deg], [AS_GEO_Latitude_Deg])
								PERSISTED			NOT	NULL,
	[AS_GEO_ChangedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__AS_GEO_Assets_Location_Posit_ChangedAt]			DEFAULT(SYSDATETIMEOFFSET()),
	[AS_GEO_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__AS_GEO_Assets_Location_Posit_RowGuid]			DEFAULT(NEWSEQUENTIALID()) ROWGUIDCOL,
																CONSTRAINT [UX__AS_GEO_Assets_Location_Posit_RowGuid]			UNIQUE NONCLUSTERED([AS_GEO_RowGuid]),
	CONSTRAINT [PK__AS_GEO_Assets_Location_Posit]	PRIMARY KEY CLUSTERED([AS_GEO_ID] ASC)
		WITH (FILLFACTOR = 95),
	CONSTRAINT [UK__AS_GEO_Assets_Location_Posit]	UNIQUE NONCLUSTERED([AS_GEO_AS_ID] ASC, [AS_GEO_ChangedAt] DESC, [AS_GEO_Checksum] ASC)
		WITH (FILLFACTOR = 95)
);