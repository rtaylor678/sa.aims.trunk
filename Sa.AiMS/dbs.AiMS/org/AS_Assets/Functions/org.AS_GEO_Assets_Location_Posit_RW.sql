﻿CREATE FUNCTION [org].[AS_GEO_Assets_Location_Posit_RW]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[p].[AS_GEO_ID],
	[p].[AS_GEO_AS_ID],

	[p].[AS_GEO_Longitude_Deg],
	[p].[AS_GEO_Latitude_Deg],
	[p].[AS_GEO_Wgs84],

	[p].[AS_GEO_Checksum],
	[p].[AS_GEO_ChangedAt]
FROM
	[org].[AS_GEO_Assets_Location_Posit]		[p]
WHERE
	[p].[AS_GEO_ChangedAt] < @ChangedBefore;