﻿CREATE VIEW [org].[AS_NME_Assets_Name_SK]
WITH SCHEMABINDING
AS
SELECT
	[p].[AS_NME_ID],
	[p].[AS_NME_AS_ID],

	[p].[AS_NME_AssetTag],
	[p].[AS_NME_AssetName],
	[p].[AS_NME_AssetDetail],

	[p].[AS_NME_Checksum],
	[p].[AS_NME_ChangedAt],
	[a].[AS_NME_PositedBy],
	[a].[AS_NME_PositedAt],
	[a].[AS_NME_PositReliability],
	[a].[AS_NME_PositReliable]

FROM
	[org].[AS_NME_Assets_Name_Posit]	[p]
INNER JOIN
	[org].[AS_NME_Assets_Name_Annex]	[a]
		ON	[a].[AS_NME_ID]		= [p].[AS_NME_ID];
GO

CREATE UNIQUE CLUSTERED INDEX [PK__AS_NME_Assets_Name_SK]
ON [org].[AS_NME_Assets_Name_SK]
(
	[AS_NME_AS_ID]				ASC,
	[AS_NME_ChangedAt]			DESC,
	[AS_NME_PositedAt]			DESC,
	[AS_NME_PositedBy]			ASC,
	[AS_NME_PositReliable]		ASC
)
WITH (FILLFACTOR = 95);
GO