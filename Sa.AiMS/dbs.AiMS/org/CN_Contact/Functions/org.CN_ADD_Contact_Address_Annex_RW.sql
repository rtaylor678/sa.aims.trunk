﻿CREATE FUNCTION [org].[CN_ADD_Contact_Address_Annex_RW]
(
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[a].[CN_ADD_ID],
	[a].[CN_ADD_PositedBy],
	[a].[CN_ADD_PositedAt],
	[a].[CN_ADD_PositReliability],
	[a].[CN_ADD_PositReliable]
FROM
	[org].[CN_ADD_Contact_Address_Annex]		[a]
WHERE
	[a].[CN_ADD_PositedAt] < @PositedBefore;