﻿CREATE FUNCTION [org].[CN_PHN_Contact_Phone_Posit_RW]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[p].[CN_PHN_ID],
	[p].[CN_PHN_CN_ID],

	[p].[CN_PHN_Type_ID],
	[p].[CN_PHN_Number],

	[p].[CN_PHN_Checksum],
	[p].[CN_PHN_ChangedAt]

FROM
	[org].[CN_PHN_Contact_Phone_Posit]		[p]
WHERE
	[p].[CN_PHN_ChangedAt] < @ChangedBefore;