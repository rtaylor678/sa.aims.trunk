﻿CREATE VIEW [org].[AS_GEO_Assets_Location_Current]
WITH SCHEMABINDING
AS
SELECT
	[GEO].[AS_GEO_ID],
	[GEO].[AS_GEO_AS_ID],

	[GEO].[AS_GEO_Longitude_Deg],
	[GEO].[AS_GEO_Latitude_Deg],
	[GEO].[AS_GEO_Wgs84],

	[GEO].[AS_GEO_Checksum],
	[GEO].[AS_GEO_ChangedAt],
	[GEO].[AS_GEO_PositedBy],
	[GEO].[AS_GEO_PositedAt],
	[GEO].[AS_GEO_PositReliability],
	[GEO].[AS_GEO_PositReliable]

FROM [org].[AS_GEO_Assets_Location_Rewind](DEFAULT, DEFAULT, DEFAULT)	[GEO];
GO

CREATE TRIGGER [org].[AS_GEO_Assets_Location_Current_Insert]
ON [org].[AS_GEO_Assets_Location_Current]
INSTEAD OF INSERT
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[AS_GEO_ID] IS NOT NULL))
		RAISERROR('INSERT into the identity column ''AS_GEO_ID'' is not allowed.', 16, 1) WITH NOWAIT;

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @GEO_Posit TABLE
	(
		[AS_GEO_ID]					INT		NOT	NULL,
		[AS_GEO_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([AS_GEO_ID] ASC),
		UNIQUE CLUSTERED([AS_GEO_Checksum] ASC)
	);

	INSERT INTO [org].[AS_GEO_Assets_Location_Posit]
	(
		[AS_GEO_AS_ID],

		[AS_GEO_Longitude_Deg],
		[AS_GEO_Latitude_Deg],

		[AS_GEO_ChangedAt]
	)
	OUTPUT
		[inserted].[AS_GEO_ID],
		[inserted].[AS_GEO_Checksum]
	INTO
		@GEO_Posit
		(
			[AS_GEO_ID],
			[AS_GEO_Checksum]
		)
	SELECT
		[i].[AS_GEO_AS_ID],

		[i].[AS_GEO_Longitude_Deg],
		[i].[AS_GEO_Latitude_Deg],

		COALESCE([i].[AS_GEO_ChangedAt], @Now)
	FROM
		[inserted]		[i];

	INSERT INTO [org].[AS_GEO_Assets_Location_Annex]
	(
		[AS_GEO_ID],
		[AS_GEO_PositedBy],
		[AS_GEO_PositedAt],
		[AS_GEO_PositReliability]
	)
	SELECT
		[p].[AS_GEO_ID],
		COALESCE([i].[AS_GEO_PositedBy], @PositedBy),
		COALESCE([i].[AS_GEO_PositedAt], @Now),
		COALESCE([i].[AS_GEO_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@GEO_Posit		[p]
			ON	[p].[AS_GEO_Checksum]	= [i].[AS_GEO_Checksum];

END;
GO

CREATE TRIGGER [org].[AS_GEO_Assets_Location_Current_Update]
ON [org].[AS_GEO_Assets_Location_Current]
INSTEAD OF UPDATE
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(UPDATE([AS_GEO_ID]))
		RAISERROR('Cannot update the identity column ''AS_GEO_ID''.', 16, 1);

	IF(UPDATE([AS_GEO_AS_ID]))
		RAISERROR('Cannot update the foreign key identity column ''AS_GEO_AS_ID''.', 16, 1);

	IF(UPDATE([AS_GEO_Checksum]))
		RAISERROR('Cannot update the computed column ''AS_GEO_Checksum''.', 16, 1);

	IF(UPDATE([AS_GEO_PositReliable]))
		RAISERROR('Cannot update the computed column ''AS_GEO_PositReliable''.', 16, 1);

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @GEO_Posit TABLE
	(
		[AS_GEO_ID]					INT		NOT	NULL,
		[AS_GEO_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([AS_GEO_ID] ASC),
		UNIQUE CLUSTERED([AS_GEO_Checksum] ASC)
	);

	INSERT INTO [org].[AS_GEO_Assets_Location_Posit]
	(
		[AS_GEO_AS_ID],
		[AS_GEO_Longitude_Deg],
		[AS_GEO_Latitude_Deg],
		[AS_GEO_ChangedAt]
	)
	OUTPUT
		[inserted].[AS_GEO_ID],
		[inserted].[AS_GEO_Checksum]
	INTO
		@GEO_Posit
		(
			[AS_GEO_ID],
			[AS_GEO_Checksum]
		)
	SELECT
		[i].[AS_GEO_AS_ID],
		[i].[AS_GEO_Longitude_Deg],
		[i].[AS_GEO_Latitude_Deg],
		COALESCE([i].[AS_GEO_ChangedAt], @Now)
	FROM
		[inserted]		[i]
	WHERE	(UPDATE([AS_GEO_Longitude_Deg])
		OR	UPDATE([AS_GEO_Latitude_Deg]))
		AND	[i].[AS_GEO_Longitude_Deg]	IS NOT NULL
		AND	[i].[AS_GEO_Latitude_Deg]	IS NOT NULL;

	INSERT INTO [org].[AS_GEO_Assets_Location_Annex]
	(
		[AS_GEO_ID],
		[AS_GEO_PositedBy],
		[AS_GEO_PositedAt],
		[AS_GEO_PositReliability]
	)
	SELECT
		[p].[AS_GEO_ID],
		COALESCE([i].[AS_GEO_PositedBy], @PositedBy),
		COALESCE([i].[AS_GEO_PositedAt], @Now),
		COALESCE([i].[AS_GEO_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@GEO_Posit		[p]
			ON	[p].[AS_GEO_Checksum]	= [i].[AS_GEO_Checksum];

	INSERT INTO [org].[AS_GEO_Assets_Location_Annex]
	(
		[AS_GEO_ID],
		[AS_GEO_PositedBy],
		[AS_GEO_PositedAt],
		[AS_GEO_PositReliability]
	)
	SELECT
		[i].[AS_GEO_ID],
		COALESCE([i].[AS_GEO_PositedBy], @PositedBy),
		COALESCE([i].[AS_GEO_PositedAt], @Now),
		COALESCE([i].[AS_GEO_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	WHERE	NOT(UPDATE([AS_GEO_Longitude_Deg])
			OR	UPDATE([AS_GEO_Latitude_Deg]));

END;
GO

CREATE TRIGGER [org].[AS_GEO_Assets_Location_Current_Delete]
ON [org].[AS_GEO_Assets_Location_Current]
INSTEAD OF DELETE
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	INSERT INTO [org].[AS_GEO_Assets_Location_Annex]
	(
		[AS_GEO_ID],
		[AS_GEO_PositedBy],
		[AS_GEO_PositedAt],
		[AS_GEO_PositReliability]
	)
	SELECT
		[d].[AS_GEO_ID],
		@PositedBy,
		@Now,
		0
	FROM
		[deleted]	[d]
	WHERE
		[d].[AS_GEO_ID]	IS NOT NULL;

END;
GO