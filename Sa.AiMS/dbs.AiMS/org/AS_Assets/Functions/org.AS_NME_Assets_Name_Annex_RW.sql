﻿CREATE FUNCTION [org].[AS_NME_Assets_Name_Annex_RW]
(
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[a].[AS_NME_ID],
	[a].[AS_NME_PositedBy],
	[a].[AS_NME_PositedAt],
	[a].[AS_NME_PositReliability],
	[a].[AS_NME_PositReliable]
FROM
	[org].[AS_NME_Assets_Name_Annex]		[a]
WHERE
	[a].[AS_NME_PositedAt] < @PositedBefore;