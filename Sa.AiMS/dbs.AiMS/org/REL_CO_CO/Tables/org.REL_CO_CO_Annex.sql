﻿CREATE TABLE [org].[REL_CO_CO_Annex]
(
	[REL_ID]				INT					NOT	NULL	CONSTRAINT [FK__REL_CO_CO_Annex_CO_NME_ID]		REFERENCES [org].[CO_NME_Company_Name_Posit]([CO_NME_ID]),

	[REL_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__REL_CO_CO_Annex_PositedBy]		DEFAULT([posit].[Get_PositorId]())
															CONSTRAINT [FK__REL_CO_CO_Annex_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[REL_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__REL_CO_CO_Annex_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[REL_PositReliability]	TINYINT				NOT	NULL,
	[REL_PositReliable]		AS CONVERT(BIT, CASE WHEN [REL_PositReliability] > 0 THEN 1 ELSE 0 END)
							PERSISTED			NOT	NULL,

	[REL_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__REL_CO_CO_Annex_RowGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
															CONSTRAINT [UX__REL_CO_CO_Annex_RowGuid]		UNIQUE NONCLUSTERED([REL_RowGuid]),

	CONSTRAINT [PK__REL_CO_CO_Annex]	PRIMARY KEY CLUSTERED([REL_ID] ASC, [REL_PositedAt] DESC, [REL_PositedBy] ASC)
		WITH (FILLFACTOR = 95)
);
