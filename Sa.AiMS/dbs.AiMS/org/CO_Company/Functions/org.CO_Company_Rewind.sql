﻿CREATE FUNCTION [org].[CO_Company_Rewind]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@Reliable		BIT					= 1
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[CO].[CO_ID],
	[CO].[CO_PositedBy],
	[CO].[CO_PositedAt],

	[NME].[CO_NME_ID],
	[NME].[CO_NME_CO_ID],

	[NME].[CO_NME_CompanyTag],
	[NME].[CO_NME_CompanyName],
	[NME].[CO_NME_CompanyDetail],

	[NME].[CO_NME_Checksum],
	[NME].[CO_NME_ChangedAt],
	[NME].[CO_NME_PositedBy],
	[NME].[CO_NME_PositedAt],
	[NME].[CO_NME_PositReliability],
	[NME].[CO_NME_PositReliable]

FROM
	[org].[CO_Company]		[CO]

LEFT OUTER JOIN
	[org].[CO_NME_Company_Name_RW](@ChangedBefore, @PositedBefore)		[NME]
		ON	[NME].[CO_NME_CO_ID]			= [CO].[CO_ID]
		AND	[NME].[CO_NME_PositReliable]	= @Reliable
		AND	[NME].[CO_NME_ID]				= (
			SELECT TOP 1
				[NME_s].[CO_NME_ID]
			FROM
				[org].[CO_NME_Company_Name_RW](@ChangedBefore, @PositedBefore)	[NME_s]
			WHERE
				[NME_s].[CO_NME_CO_ID]		= [NME].[CO_NME_CO_ID]
			ORDER BY
				[NME_s].[CO_NME_ChangedAt]	DESC,
				[NME_s].[CO_NME_PositedAt]	DESC
			);