﻿CREATE TABLE [org].[PE_People]
(
	[PE_ID]						INT					NOT	NULL	IDENTITY(1, 1),

	[PE_PositedBy]				INT					NOT	NULL	CONSTRAINT [DF__PE_People_PositedBy]	DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__PE_People_PositedBy]	REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[PE_PositedAt]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__PE_People_PositedAt]	DEFAULT(SYSDATETIMEOFFSET()),
	[PE_RowGuid]				UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__PE_People_RowGuid]		DEFAULT(NEWSEQUENTIALID()) ROWGUIDCOL,
																CONSTRAINT [UX__PE_People_RowGuid]		UNIQUE NONCLUSTERED([PE_RowGuid]),
	CONSTRAINT [PK__PE_People]	PRIMARY KEY CLUSTERED([PE_ID] ASC)
		WITH (FILLFACTOR = 95)
);