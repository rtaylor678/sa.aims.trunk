﻿CREATE FUNCTION [org].[CN_PHN_Contact_Phone_Annex_RW]
(
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[a].[CN_PHN_ID],
	[a].[CN_PHN_PositedBy],
	[a].[CN_PHN_PositedAt],
	[a].[CN_PHN_PositReliability],
	[a].[CN_PHN_PositReliable]
FROM
	[org].[CN_PHN_Contact_Phone_Annex]		[a]
WHERE
	[a].[CN_PHN_PositedAt] < @PositedBefore;