﻿CREATE VIEW [org].[CN_ADD_Contact_Address_Current]
WITH SCHEMABINDING
AS
SELECT
	[ADD].[CN_ADD_ID],
	[ADD].[CN_ADD_CN_ID],

	[ADD].[CN_ADD_Type_ID],
	[ADD].[CN_ADD_Country_ID],
	[ADD].[CN_ADD_State],
	[ADD].[CN_ADD_City],
	[ADD].[CN_ADD_PostalCode],
	[ADD].[CN_ADD_PoBox],
	[ADD].[CN_ADD_Address1],
	[ADD].[CN_ADD_Address2],
	[ADD].[CN_ADD_Address3],

	[ADD].[CN_ADD_Checksum],
	[ADD].[CN_ADD_ChangedAt],
	[ADD].[CN_ADD_PositedBy],
	[ADD].[CN_ADD_PositedAt],
	[ADD].[CN_ADD_PositReliability],
	[ADD].[CN_ADD_PositReliable]

FROM [org].[CN_ADD_Contact_Address_Rewind](DEFAULT, DEFAULT, DEFAULT)	[ADD];
GO


CREATE TRIGGER [org].[CN_ADD_Contact_Address_Current_Insert]
ON [org].[CN_ADD_Contact_Address_Current]
INSTEAD OF INSERT
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CN_ADD_ID] IS NOT NULL))
		RAISERROR('INSERT into the identity column ''CN_ADD_ID'' is not allowed.', 16, 1) WITH NOWAIT;
		--	THROW 50000, N'INSERT into the identity column ''CN_ADD_ID'' is not allowed.', 1;

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @ADD_Posit TABLE
	(
		[CN_ADD_ID]					INT		NOT	NULL,
		[CN_ADD_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([CN_ADD_ID] ASC),
		UNIQUE CLUSTERED([CN_ADD_Checksum] ASC)
	);

	INSERT INTO [org].[CN_ADD_Contact_Address_Posit]
	(
		[CN_ADD_CN_ID],

		[CN_ADD_Type_ID],
		[CN_ADD_Country_ID],
		[CN_ADD_State],
		[CN_ADD_City],
		[CN_ADD_PostalCode],
		[CN_ADD_PoBox],
		[CN_ADD_Address1],
		[CN_ADD_Address2],
		[CN_ADD_Address3],

		[CN_ADD_ChangedAt]
	)
	OUTPUT
		[inserted].[CN_ADD_ID],
		[inserted].[CN_ADD_Checksum]
	INTO
		@ADD_Posit
		(
			[CN_ADD_ID],
			[CN_ADD_Checksum]
		)
	SELECT
		[i].[CN_ADD_CN_ID],

		[i].[CN_ADD_Type_ID],
		[i].[CN_ADD_Country_ID],
		[i].[CN_ADD_State],
		[i].[CN_ADD_City],
		[i].[CN_ADD_PostalCode],
		[i].[CN_ADD_PoBox],
		[i].[CN_ADD_Address1],
		[i].[CN_ADD_Address2],
		[i].[CN_ADD_Address3],

		COALESCE([i].[CN_ADD_ChangedAt], @Now)
	FROM
		[inserted]		[i];

	INSERT INTO [org].[CN_ADD_Contact_Address_Annex]
	(
		[CN_ADD_ID],
		[CN_ADD_PositedBy],
		[CN_ADD_PositedAt],
		[CN_ADD_PositReliability]
	)
	SELECT
		[p].[CN_ADD_ID],
		COALESCE([i].[CN_ADD_PositedBy], @PositedBy),
		COALESCE([i].[CN_ADD_PositedAt], @Now),
		COALESCE([i].[CN_ADD_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@ADD_Posit		[p]
			ON	[p].[CN_ADD_Checksum]	= [i].[CN_ADD_Checksum];

END;
GO

CREATE TRIGGER [org].[CN_ADD_Contact_Address_Current_Update]
ON [org].[CN_ADD_Contact_Address_Current]
INSTEAD OF UPDATE
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(UPDATE([CN_ADD_ID]))
		RAISERROR('Cannot update the identity column ''CN_ADD_ID''.', 16, 1);
		--	THROW 50000, N'Cannot update the identity column ''CN_ADD_ID''.', 1;

	IF(UPDATE([CN_ADD_CN_ID]))
		RAISERROR('Cannot update the foreign key identity column ''CN_ADD_ID''.', 16, 1);
		--	THROW 50000, N'Cannot update the foreign key identity column ''CN_ADD_ID''.', 1;

	IF(UPDATE([CN_ADD_Checksum]))
		RAISERROR('Cannot update the computed column ''CN_ADD_Checksum''.', 16, 1);
		--	THROW 50000, N'Cannot update the computed column ''CN_ADD_Checksum''.', 1;

	IF(UPDATE([CN_ADD_PositReliable]))
		RAISERROR('Cannot update the computed column ''CN_ADD_PositReliable''.', 16, 1);
		--	THROW 50000, N'Cannot update the computed column ''CN_ADD_PositReliable''.', 1;

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @ADD_Posit TABLE
	(
		[CN_ADD_ID]					INT		NOT	NULL,
		[CN_ADD_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([CN_ADD_ID] ASC),
		UNIQUE CLUSTERED([CN_ADD_Checksum] ASC)
	);

	INSERT INTO [org].[CN_ADD_Contact_Address_Posit]
	(
		[CN_ADD_CN_ID],

		[CN_ADD_Type_ID],
		[CN_ADD_Country_ID],
		[CN_ADD_State],
		[CN_ADD_City],
		[CN_ADD_PostalCode],
		[CN_ADD_PoBox],
		[CN_ADD_Address1],
		[CN_ADD_Address2],
		[CN_ADD_Address3],

		[CN_ADD_ChangedAt]
	)
	OUTPUT
		[inserted].[CN_ADD_ID],
		[inserted].[CN_ADD_Checksum]
	INTO
		@ADD_Posit
		(
			[CN_ADD_ID],
			[CN_ADD_Checksum]
		)
	SELECT
		[i].[CN_ADD_CN_ID],

		[i].[CN_ADD_Type_ID],
		[i].[CN_ADD_Country_ID],
		[i].[CN_ADD_State],
		[i].[CN_ADD_City],
		[i].[CN_ADD_PostalCode],
		[i].[CN_ADD_PoBox],
		[i].[CN_ADD_Address1],
		[i].[CN_ADD_Address2],
		[i].[CN_ADD_Address3],

		COALESCE([i].[CN_ADD_ChangedAt], @Now)
	FROM
		[inserted]		[i]
	WHERE	UPDATE([CN_ADD_Type_ID])
		OR	UPDATE([CN_ADD_Country_ID])
		OR	UPDATE([CN_ADD_State])
		OR	UPDATE([CN_ADD_City])
		OR	UPDATE([CN_ADD_PostalCode])
		OR	UPDATE([CN_ADD_PoBox])
		OR	UPDATE([CN_ADD_Address1])
		OR	UPDATE([CN_ADD_Address2])
		OR	UPDATE([CN_ADD_Address3]);

	INSERT INTO [org].[CN_ADD_Contact_Address_Annex]
	(
		[CN_ADD_ID],
		[CN_ADD_PositedBy],
		[CN_ADD_PositedAt],
		[CN_ADD_PositReliability]
	)
	SELECT
		[p].[CN_ADD_ID],
		COALESCE([i].[CN_ADD_PositedBy], @PositedBy),
		COALESCE([i].[CN_ADD_PositedAt], @Now),
		COALESCE([i].[CN_ADD_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@ADD_Posit		[p]
			ON	[p].[CN_ADD_Checksum]	= BINARY_CHECKSUM([i].[CN_ADD_Type_ID], [i].[CN_ADD_Country_ID], [i].[CN_ADD_State], [i].[CN_ADD_City], [i].[CN_ADD_PostalCode], [i].[CN_ADD_PoBox], [i].[CN_ADD_Address1], [i].[CN_ADD_Address2], [i].[CN_ADD_Address3]);

	INSERT INTO [org].[CN_ADD_Contact_Address_Annex]
	(
		[CN_ADD_ID],
		[CN_ADD_PositedBy],
		[CN_ADD_PositedAt],
		[CN_ADD_PositReliability]
	)
	SELECT
		[i].[CN_ADD_ID],
		COALESCE([i].[CN_ADD_PositedBy], @PositedBy),
		COALESCE([i].[CN_ADD_PositedAt], @Now),
		COALESCE([i].[CN_ADD_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	WHERE	NOT(UPDATE([CN_ADD_Type_ID])
			OR	UPDATE([CN_ADD_Country_ID])
			OR	UPDATE([CN_ADD_State])
			OR	UPDATE([CN_ADD_City])
			OR	UPDATE([CN_ADD_PostalCode])
			OR	UPDATE([CN_ADD_PoBox])
			OR	UPDATE([CN_ADD_Address1])
			OR	UPDATE([CN_ADD_Address2])
			OR	UPDATE([CN_ADD_Address3]));

END;
GO

CREATE TRIGGER [org].[CN_ADD_Contact_Address_Current_Delete]
ON [org].[CN_ADD_Contact_Address_Current]
INSTEAD OF DELETE
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	INSERT INTO [org].[CN_ADD_Contact_Address_Annex]
	(
		[CN_ADD_ID],
		[CN_ADD_PositedBy],
		[CN_ADD_PositedAt],
		[CN_ADD_PositReliability]
	)
	SELECT
		[d].[CN_ADD_ID],
		@PositedBy,
		@Now,
		0
	FROM
		[deleted]	[d]
	WHERE
		[d].[CN_ADD_ID]	IS NOT NULL

END;
GO