﻿CREATE FUNCTION [org].[CN_ADD_Contact_Address_Posit_RW]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[p].[CN_ADD_ID],
	[p].[CN_ADD_CN_ID],

	[p].[CN_ADD_Type_ID],
	[p].[CN_ADD_Country_ID],
	[p].[CN_ADD_State],
	[p].[CN_ADD_City],
	[p].[CN_ADD_PostalCode],
	[p].[CN_ADD_PoBox],
	[p].[CN_ADD_Address1],
	[p].[CN_ADD_Address2],
	[p].[CN_ADD_Address3],

	[p].[CN_ADD_Checksum],
	[p].[CN_ADD_ChangedAt]

FROM
	[org].[CN_ADD_Contact_Address_Posit]		[p]
WHERE
	[p].[CN_ADD_ChangedAt] < @ChangedBefore;