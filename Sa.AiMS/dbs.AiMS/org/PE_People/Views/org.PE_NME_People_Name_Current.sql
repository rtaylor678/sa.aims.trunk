﻿CREATE VIEW [org].[PE_NME_People_Name_Current]
WITH SCHEMABINDING
AS
SELECT
	[NME].[PE_NME_ID],
	[NME].[PE_NME_PE_ID],

	[NME].[PE_NME_NamePrefix],
	[NME].[PE_NME_NameFirst],
	[NME].[PE_NME_NameMiddle],
	[NME].[PE_NME_NameLast],
	[NME].[PE_NME_NameSuffix],
	[NME].[PE_NME_Title],

	[NME].[PE_NME_Checksum],
	[NME].[PE_NME_ChangedAt],
	[NME].[PE_NME_PositedBy],
	[NME].[PE_NME_PositedAt],
	[NME].[PE_NME_PositReliability],
	[NME].[PE_NME_PositReliable]

FROM [org].[PE_NME_People_Name_Rewind](DEFAULT, DEFAULT, DEFAULT)	[NME];
GO

CREATE TRIGGER [org].[PE_NME_People_Name_Current_Insert]
ON [org].[PE_NME_People_Name_Current]
INSTEAD OF INSERT
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[PE_NME_ID] IS NOT NULL))
		RAISERROR('INSERT into the identity column ''PE_NME_ID'' is not allowed.', 16, 1) WITH NOWAIT;

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @NME_Posit TABLE
	(
		[PE_NME_ID]					INT		NOT	NULL,
		[PE_NME_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([PE_NME_ID] ASC),
		UNIQUE CLUSTERED([PE_NME_Checksum] ASC)
	);

	INSERT INTO [org].[PE_NME_People_Name_Posit]
	(
		[PE_NME_PE_ID],

		[PE_NME_NamePrefix],
		[PE_NME_NameFirst],
		[PE_NME_NameMiddle],
		[PE_NME_NameLast],
		[PE_NME_NameSuffix],
		[PE_NME_Title],

		[PE_NME_ChangedAt]
	)
	OUTPUT
		[inserted].[PE_NME_ID],
		[inserted].[PE_NME_Checksum]
	INTO
		@NME_Posit
		(
			[PE_NME_ID],
			[PE_NME_Checksum]
		)
	SELECT
		[i].[PE_NME_PE_ID],

		[i].[PE_NME_NamePrefix],
		[i].[PE_NME_NameFirst],
		[i].[PE_NME_NameMiddle],
		[i].[PE_NME_NameLast],
		[i].[PE_NME_NameSuffix],
		[i].[PE_NME_Title],

		COALESCE([i].[PE_NME_ChangedAt], @Now)
	FROM
		[inserted]		[i];

	INSERT INTO [org].[PE_NME_People_Name_Annex]
	(
		[PE_NME_ID],
		[PE_NME_PositedBy],
		[PE_NME_PositedAt],
		[PE_NME_PositReliability]
	)
	SELECT
		[p].[PE_NME_ID],
		COALESCE([i].[PE_NME_PositedBy], @PositedBy),
		COALESCE([i].[PE_NME_PositedAt], @Now),
		COALESCE([i].[PE_NME_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@NME_Posit		[p]
			ON	[p].[PE_NME_Checksum]	= [i].[PE_NME_Checksum];

END;
GO

CREATE TRIGGER [org].[PE_NME_People_Name_Current_Update]
ON [org].[PE_NME_People_Name_Current]
INSTEAD OF UPDATE
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(UPDATE([PE_NME_ID]))
		RAISERROR('Cannot update the identity column ''PE_NME_ID''.', 16, 1);

	IF(UPDATE([PE_NME_PE_ID]))
		RAISERROR('Cannot update the foreign key identity column ''PE_NME_PE_ID''.', 16, 1);

	IF(UPDATE([PE_NME_Checksum]))
		RAISERROR('Cannot update the computed column ''PE_NME_Checksum''.', 16, 1);

	IF(UPDATE([PE_NME_PositReliable]))
		RAISERROR('Cannot update the computed column ''PE_NME_PositReliable''.', 16, 1);

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @NME_Posit TABLE
	(
		[PE_NME_ID]					INT		NOT	NULL,
		[PE_NME_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([PE_NME_ID] ASC),
		UNIQUE CLUSTERED([PE_NME_Checksum] ASC)
	);

	INSERT INTO [org].[PE_NME_People_Name_Posit]
	(
		[PE_NME_PE_ID],
		[PE_NME_NamePrefix],
		[PE_NME_NameFirst],
		[PE_NME_NameMiddle],
		[PE_NME_NameLast],
		[PE_NME_NameSuffix],
		[PE_NME_Title],
		[PE_NME_ChangedAt]
	)
	OUTPUT
		[inserted].[PE_NME_ID],
		[inserted].[PE_NME_Checksum]
	INTO
		@NME_Posit
		(
			[PE_NME_ID],
			[PE_NME_Checksum]
		)
	SELECT
		[i].[PE_NME_PE_ID],
		[i].[PE_NME_NamePrefix],
		[i].[PE_NME_NameFirst],
		[i].[PE_NME_NameMiddle],
		[i].[PE_NME_NameLast],
		[i].[PE_NME_NameSuffix],
		[i].[PE_NME_Title],
		COALESCE([i].[PE_NME_ChangedAt], @Now)
	FROM
		[inserted]		[i]
	WHERE	UPDATE([PE_NME_NamePrefix])
		OR	UPDATE([PE_NME_NameFirst])
		OR	UPDATE([PE_NME_NameMiddle])
		OR	UPDATE([PE_NME_NameLast])
		OR	UPDATE([PE_NME_NameSuffix])
		OR	UPDATE([PE_NME_Title]);

	INSERT INTO [org].[PE_NME_People_Name_Annex]
	(
		[PE_NME_ID],
		[PE_NME_PositedBy],
		[PE_NME_PositedAt],
		[PE_NME_PositReliability]
	)
	SELECT
		[p].[PE_NME_ID],
		COALESCE([i].[PE_NME_PositedBy], @PositedBy),
		COALESCE([i].[PE_NME_PositedAt], @Now),
		COALESCE([i].[PE_NME_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@NME_Posit		[p]
			ON	[p].[PE_NME_Checksum]	= BINARY_CHECKSUM([i].[PE_NME_NamePrefix], [i].[PE_NME_NameFirst], [i].[PE_NME_NameMiddle], [i].[PE_NME_NameLast], [i].[PE_NME_NameSuffix], [i].[PE_NME_Title])

	INSERT INTO [org].[PE_NME_People_Name_Annex]
	(
		[PE_NME_ID],
		[PE_NME_PositedBy],
		[PE_NME_PositedAt],
		[PE_NME_PositReliability]
	)
	SELECT
		[i].[PE_NME_ID],
		COALESCE([i].[PE_NME_PositedBy], @PositedBy),
		COALESCE([i].[PE_NME_PositedAt], @Now),
		COALESCE([i].[PE_NME_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	WHERE	NOT(UPDATE([PE_NME_NamePrefix])
			OR	UPDATE([PE_NME_NameFirst])
			OR	UPDATE([PE_NME_NameMiddle])
			OR	UPDATE([PE_NME_NameLast])
			OR	UPDATE([PE_NME_NameSuffix])
			OR	UPDATE([PE_NME_Title]));

END;
GO

CREATE TRIGGER [org].[PE_NME_People_Name_Current_Delete]
ON [org].[PE_NME_People_Name_Current]
INSTEAD OF DELETE
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	INSERT INTO [org].[PE_NME_People_Name_Annex]
	(
		[PE_NME_ID],
		[PE_NME_PositedBy],
		[PE_NME_PositedAt],
		[PE_NME_PositReliability]
	)
	SELECT
		[d].[PE_NME_ID],
		@PositedBy,
		@Now,
		0
	FROM
		[deleted]	[d]
	WHERE
		[d].[PE_NME_ID]	IS NOT NULL;

END;
GO