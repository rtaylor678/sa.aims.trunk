﻿CREATE FUNCTION [org].[AS_NME_Assets_Name_Posit_RW]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[p].[AS_NME_ID],
	[p].[AS_NME_AS_ID],

	[p].[AS_NME_AssetTag],
	[p].[AS_NME_AssetName],
	[p].[AS_NME_AssetDetail],

	[p].[AS_NME_Checksum],
	[p].[AS_NME_ChangedAt]
FROM
	[org].[AS_NME_Assets_Name_Posit]		[p]
WHERE
	[p].[AS_NME_ChangedAt] < @ChangedBefore;