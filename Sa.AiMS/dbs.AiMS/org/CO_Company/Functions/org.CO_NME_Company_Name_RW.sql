﻿CREATE FUNCTION [org].[CO_NME_Company_Name_RW]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[p].[CO_NME_ID],
	[p].[CO_NME_CO_ID],

	[p].[CO_NME_CompanyTag],
	[p].[CO_NME_CompanyName],
	[p].[CO_NME_CompanyDetail],

	[p].[CO_NME_Checksum],
	[p].[CO_NME_ChangedAt],
	[a].[CO_NME_PositedBy],
	[a].[CO_NME_PositedAt],
	[a].[CO_NME_PositReliability],
	[a].[CO_NME_PositReliable]

FROM
	[org].[CO_NME_Company_Name_Posit_RW](@ChangedBefore)	[p]

INNER JOIN
	[org].[CO_NME_Company_Name_Annex_RW](@PositedBefore)	[a]
		ON	[a].[CO_NME_ID]				= [p].[CO_NME_ID]
		AND	[a].[CO_NME_PositedAt]		= (
			SELECT TOP 1
				[s].[CO_NME_PositedAt]
			FROM
				[org].[CO_NME_Company_Name_Annex_RW](@PositedBefore)	[s]
			WHERE
				[s].[CO_NME_ID]			= [a].[CO_NME_ID]
			ORDER BY
				[s].[CO_NME_PositedAt]	DESC
			);