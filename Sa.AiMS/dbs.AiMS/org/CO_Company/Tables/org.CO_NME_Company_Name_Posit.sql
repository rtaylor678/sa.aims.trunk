﻿CREATE TABLE [org].[CO_NME_Company_Name_Posit]
(
	[CO_NME_ID]					INT					NOT	NULL	IDENTITY(1, 1),
	[CO_NME_CO_ID]				INT					NOT	NULL	CONSTRAINT [FK__CO_NME_Company_Name_Posit_CO_NME_CO_ID]				REFERENCES [org].[CO_Company]([CO_ID]),

	[CO_NME_CompanyTag]			VARCHAR(12)			NOT	NULL	CONSTRAINT [CL__CO_NME_Company_Name_Posit_CO_NME_CompanyTag]		CHECK([CO_NME_CompanyTag] <> ''),
	[CO_NME_CompanyName]		NVARCHAR(24)		NOT	NULL	CONSTRAINT [CL__CO_NME_Company_Name_Posit_CO_NME_CompanyName]		CHECK([CO_NME_CompanyName] <> ''),
	[CO_NME_CompanyDetail]		NVARCHAR(48)		NOT	NULL	CONSTRAINT [CL__CO_NME_Company_Name_Posit_CO_NME_CompanyDetail]		CHECK([CO_NME_CompanyDetail] <> ''),
	[CO_NME_Checksum]			AS BINARY_CHECKSUM([CO_NME_CompanyTag], [CO_NME_CompanyName], [CO_NME_CompanyDetail])
								PERSISTED			NOT	NULL,
	[CO_NME_ChangedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__CO_NME_Company_Name_Posit_ChangedAt]				DEFAULT(SYSDATETIMEOFFSET()),
	[CO_NME_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__CO_NME_Company_Name_Posit_RowGuid]					DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__CO_NME_Company_Name_Posit_RowGuid]					UNIQUE NONCLUSTERED([CO_NME_RowGuid]),
	CONSTRAINT [PK__CO_NME_Company_Name_Posit]		PRIMARY KEY CLUSTERED([CO_NME_ID] ASC)
		WITH (FILLFACTOR = 95),
	CONSTRAINT [UK__CO_NME_Company_Name_Posit]		UNIQUE NONCLUSTERED([CO_NME_CO_ID] ASC, [CO_NME_ChangedAt] DESC, [CO_NME_Checksum] ASC)
		WITH (FILLFACTOR = 95)
);