﻿CREATE FUNCTION [org].[AS_GEO_Assets_Location_Rewind]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@Reliable		BIT					= 1
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[GEO].[AS_GEO_ID],
	[GEO].[AS_GEO_AS_ID],

	[GEO].[AS_GEO_Longitude_Deg],
	[GEO].[AS_GEO_Latitude_Deg],
	[GEO].[AS_GEO_Wgs84],

	[GEO].[AS_GEO_Checksum],
	[GEO].[AS_GEO_ChangedAt],
	[GEO].[AS_GEO_PositedBy],
	[GEO].[AS_GEO_PositedAt],
	[GEO].[AS_GEO_PositReliability],
	[GEO].[AS_GEO_PositReliable]

FROM
	[org].[AS_GEO_Assets_Location_RW](@ChangedBefore, @PositedBefore)			[GEO]

WHERE	[GEO].[AS_GEO_PositReliable]	= @Reliable
	AND	[GEO].[AS_GEO_ID]				= (
		SELECT TOP 1
			[GEO_s].[AS_GEO_ID]
		FROM
			[org].[AS_GEO_Assets_Location_RW](@ChangedBefore, @PositedBefore)	[GEO_s]
		WHERE
			[GEO_s].[AS_GEO_AS_ID]		= [GEO].[AS_GEO_AS_ID]
		ORDER BY
			[GEO_s].[AS_GEO_ChangedAt]	DESC,
			[GEO_s].[AS_GEO_PositedAt]	DESC
		);