﻿CREATE FUNCTION [org].[CN_EML_Contact_EMail_Posit_RW]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[p].[CN_EML_ID],
	[p].[CN_EML_CN_ID],

	[p].[CN_EML_Type_ID],
	[p].[CN_EML_Address],

	[p].[CN_EML_Checksum],
	[p].[CN_EML_ChangedAt]

FROM
	[org].[CN_EML_Contact_EMail_Posit]		[p]
WHERE
	[p].[CN_EML_ChangedAt] < @ChangedBefore;