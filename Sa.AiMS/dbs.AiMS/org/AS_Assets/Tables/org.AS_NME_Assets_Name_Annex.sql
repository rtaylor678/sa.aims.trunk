﻿CREATE TABLE [org].[AS_NME_Assets_Name_Annex]
(
	[AS_NME_ID]					INT					NOT	NULL	CONSTRAINT [FK__AS_NME_Assets_Name_Annex_AS_NME_ID]		REFERENCES [org].[AS_NME_Assets_Name_Posit]([AS_NME_ID]),

	[AS_NME_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__AS_NME_Assets_Name_Annex_PositedBy]		DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__AS_NME_Assets_Name_Annex_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[AS_NME_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__AS_NME_Assets_Name_Annex_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[AS_NME_PositReliability]	TINYINT				NOT	NULL	CONSTRAINT [DF__AS_NME_Assets_Name_Annex_PositRel]		DEFAULT(50),
	[AS_NME_PositReliable]		AS CONVERT(BIT, CASE WHEN [AS_NME_PositReliability] > 0 THEN 1 ELSE 0 END)
								PERSISTED			NOT	NULL,
	[AS_NME_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__AS_NME_Assets_Name_Annex_RowGuid]		DEFAULT(NEWSEQUENTIALID()) ROWGUIDCOL,
																CONSTRAINT [UX__AS_NME_Assets_Name_Annex_RowGuid]		UNIQUE NONCLUSTERED([AS_NME_RowGuid]),
	CONSTRAINT [PK__AS_NME_Assets_Name_Annex]		PRIMARY KEY CLUSTERED([AS_NME_ID] ASC, [AS_NME_PositedAt] DESC, [AS_NME_PositedBy] ASC)
		WITH (FILLFACTOR = 95)
);