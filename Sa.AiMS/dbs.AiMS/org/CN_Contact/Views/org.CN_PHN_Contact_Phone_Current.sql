﻿CREATE VIEW [org].[CN_PHN_Contact_Phone_Current]
WITH SCHEMABINDING
AS
SELECT
	[PHN].[CN_PHN_ID],
	[PHN].[CN_PHN_CN_ID],

	[PHN].[CN_PHN_Type_ID],
	[PHN].[CN_PHN_Number],

	[PHN].[CN_PHN_Checksum],
	[PHN].[CN_PHN_ChangedAt],
	[PHN].[CN_PHN_PositedBy],
	[PHN].[CN_PHN_PositedAt],
	[PHN].[CN_PHN_PositReliability],
	[PHN].[CN_PHN_PositReliable]

FROM [org].[CN_PHN_Contact_Phone_Rewind](DEFAULT, DEFAULT, DEFAULT)	[PHN];
GO

CREATE TRIGGER [org].[CN_PHN_Contact_Phone_Current_Insert]
ON [org].[CN_PHN_Contact_Phone_Current]
INSTEAD OF INSERT
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[CN_PHN_ID] IS NOT NULL))
		RAISERROR('INSERT into the identity column ''CN_PHN_ID'' is not allowed.', 16, 1) WITH NOWAIT;
		--	THROW 50000, N'INSERT into the identity column ''CN_PHN_ID'' is not allowed.', 1;

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @PHN_Posit TABLE
	(
		[CN_PHN_ID]					INT		NOT	NULL,
		[CN_PHN_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([CN_PHN_ID] ASC),
		UNIQUE CLUSTERED([CN_PHN_Checksum] ASC)
	);

	INSERT INTO [org].[CN_PHN_Contact_Phone_Posit]
	(
		[CN_PHN_CN_ID],

		[CN_PHN_Type_ID],
		[CN_PHN_Number],

		[CN_PHN_ChangedAt]
	)
	OUTPUT
		[inserted].[CN_PHN_ID],
		[inserted].[CN_PHN_Checksum]
	INTO
		@PHN_Posit
		(
			[CN_PHN_ID],
			[CN_PHN_Checksum]
		)
	SELECT
		[i].[CN_PHN_CN_ID],

		[i].[CN_PHN_Type_ID],
		[i].[CN_PHN_Number],

		COALESCE([i].[CN_PHN_ChangedAt], @Now)
	FROM
		[inserted]		[i];

	INSERT INTO [org].[CN_PHN_Contact_Phone_Annex]
	(
		[CN_PHN_ID],
		[CN_PHN_PositedBy],
		[CN_PHN_PositedAt],
		[CN_PHN_PositReliability]
	)
	SELECT
		[p].[CN_PHN_ID],
		COALESCE([i].[CN_PHN_PositedBy], @PositedBy),
		COALESCE([i].[CN_PHN_PositedAt], @Now),
		COALESCE([i].[CN_PHN_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@PHN_Posit		[p]
			ON	[p].[CN_PHN_Checksum]	= [i].[CN_PHN_Checksum];

END;
GO

CREATE TRIGGER [org].[CN_PHN_Contact_Phone_Current_Update]
ON [org].[CN_PHN_Contact_Phone_Current]
INSTEAD OF UPDATE
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(UPDATE([CN_PHN_ID]))
		RAISERROR('Cannot update the identity column ''CN_PHN_ID''.', 16, 1);
		--	THROW 50000, N'Cannot update the identity column ''CN_PHN_ID''.', 1;

	IF(UPDATE([CN_PHN_CN_ID]))
		RAISERROR('Cannot update the foreign key identity column ''CN_PHN_ID''.', 16, 1);
		--	THROW 50000, N'Cannot update the foreign key identity column ''CN_PHN_ID''.', 1;

	IF(UPDATE([CN_PHN_Checksum]))
		RAISERROR('Cannot update the computed column ''CN_PHN_Checksum''.', 16, 1);
		--	THROW 50000, N'Cannot update the computed column ''CN_PHN_Checksum''.', 1;

	IF(UPDATE([CN_PHN_PositReliable]))
		RAISERROR('Cannot update the computed column ''CN_PHN_PositReliable''.', 16, 1);
		--	THROW 50000, N'Cannot update the computed column ''CN_PHN_PositReliable''.', 1;

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @PHN_Posit TABLE
	(
		[CN_PHN_ID]					INT		NOT	NULL,
		[CN_PHN_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([CN_PHN_ID] ASC),
		UNIQUE CLUSTERED([CN_PHN_Checksum] ASC)
	);

	INSERT INTO [org].[CN_PHN_Contact_Phone_Posit]
	(
		[CN_PHN_CN_ID],

		[CN_PHN_Type_ID],
		[CN_PHN_Number],

		[CN_PHN_ChangedAt]
	)
	OUTPUT
		[inserted].[CN_PHN_ID],
		[inserted].[CN_PHN_Checksum]
	INTO
		@PHN_Posit
		(
			[CN_PHN_ID],
			[CN_PHN_Checksum]
		)
	SELECT
		[i].[CN_PHN_CN_ID],

		[i].[CN_PHN_Type_ID],
		[i].[CN_PHN_Number],

		COALESCE([i].[CN_PHN_ChangedAt], @Now)
	FROM
		[inserted]		[i]
	WHERE	UPDATE([CN_PHN_Type_ID])
		OR	UPDATE([CN_PHN_Number]);

	INSERT INTO [org].[CN_PHN_Contact_Phone_Annex]
	(
		[CN_PHN_ID],
		[CN_PHN_PositedBy],
		[CN_PHN_PositedAt],
		[CN_PHN_PositReliability]
	)
	SELECT
		[p].[CN_PHN_ID],
		COALESCE([i].[CN_PHN_PositedBy], @PositedBy),
		COALESCE([i].[CN_PHN_PositedAt], @Now),
		COALESCE([i].[CN_PHN_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@PHN_Posit		[p]
			ON	[p].[CN_PHN_Checksum]	= BINARY_CHECKSUM([i].[CN_PHN_Type_ID], [i].[CN_PHN_Number]);

	INSERT INTO [org].[CN_PHN_Contact_Phone_Annex]
	(
		[CN_PHN_ID],
		[CN_PHN_PositedBy],
		[CN_PHN_PositedAt],
		[CN_PHN_PositReliability]
	)
	SELECT
		[i].[CN_PHN_ID],
		COALESCE([i].[CN_PHN_PositedBy], @PositedBy),
		COALESCE([i].[CN_PHN_PositedAt], @Now),
		COALESCE([i].[CN_PHN_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	WHERE	NOT(UPDATE([CN_PHN_Type_ID])
			OR	UPDATE([CN_PHN_Number]));

END;
GO

CREATE TRIGGER [org].[CN_PHN_Contact_Phone_Current_Delete]
ON [org].[CN_PHN_Contact_Phone_Current]
INSTEAD OF DELETE
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	INSERT INTO [org].[CN_PHN_Contact_Phone_Annex]
	(
		[CN_PHN_ID],
		[CN_PHN_PositedBy],
		[CN_PHN_PositedAt],
		[CN_PHN_PositReliability]
	)
	SELECT
		[d].[CN_PHN_ID],
		@PositedBy,
		@Now,
		0
	FROM
		[deleted]	[d]
	WHERE
		[d].[CN_PHN_ID]	IS NOT NULL

END;
GO
