﻿CREATE VIEW [org].[CN_EML_Contact_EMail_SK]
WITH SCHEMABINDING
AS
SELECT
	[p].[CN_EML_ID],
	[p].[CN_EML_CN_ID],

	[p].[CN_EML_Type_ID],
	[p].[CN_EML_Address],

	[p].[CN_EML_Checksum],
	[p].[CN_EML_ChangedAt],
	[a].[CN_EML_PositedBy],
	[a].[CN_EML_PositedAt],
	[a].[CN_EML_PositReliability],
	[a].[CN_EML_PositReliable]

FROM
	[org].[CN_EML_Contact_EMail_Posit]	[p]
INNER JOIN
	[org].[CN_EML_Contact_EMail_Annex]	[a]
		ON	[a].[CN_EML_ID]		= [p].[CN_EML_ID];
GO

CREATE UNIQUE CLUSTERED INDEX [PK__CN_EML_Contact_EMail_SK]
ON [org].[CN_EML_Contact_EMail_SK]
(
	[CN_EML_CN_ID]				ASC,
	[CN_EML_ChangedAt]			DESC,
	[CN_EML_PositedAt]			DESC,
	[CN_EML_PositedBy]			ASC,
	[CN_EML_PositReliable]		ASC
)
WITH (FILLFACTOR = 95);
GO