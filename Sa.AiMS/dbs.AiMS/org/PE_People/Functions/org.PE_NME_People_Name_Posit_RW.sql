﻿CREATE FUNCTION [org].[PE_NME_People_Name_Posit_RW]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[p].[PE_NME_ID],
	[p].[PE_NME_PE_ID],

	[p].[PE_NME_NamePrefix],
	[p].[PE_NME_NameFirst],
	[p].[PE_NME_NameMiddle],
	[p].[PE_NME_NameLast],
	[p].[PE_NME_NameSuffix],
	[p].[PE_NME_Title],

	[p].[PE_NME_Checksum],
	[p].[PE_NME_ChangedAt]

FROM
	[org].[PE_NME_People_Name_Posit]		[p]
WHERE
	[p].[PE_NME_ChangedAt] < @ChangedBefore;