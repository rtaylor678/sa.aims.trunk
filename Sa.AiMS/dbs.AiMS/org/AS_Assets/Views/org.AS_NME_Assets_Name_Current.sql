﻿CREATE VIEW [org].[AS_NME_Assets_Name_Current]
WITH SCHEMABINDING
AS
SELECT
	[NME].[AS_NME_ID],
	[NME].[AS_NME_AS_ID],

	[NME].[AS_NME_AssetTag],
	[NME].[AS_NME_AssetName],
	[NME].[AS_NME_AssetDetail],

	[NME].[AS_NME_Checksum],
	[NME].[AS_NME_ChangedAt],
	[NME].[AS_NME_PositedBy],
	[NME].[AS_NME_PositedAt],
	[NME].[AS_NME_PositReliability],
	[NME].[AS_NME_PositReliable]

FROM [org].[AS_NME_Assets_Name_Rewind](DEFAULT, DEFAULT, DEFAULT)	[NME];
GO

CREATE TRIGGER [org].[AS_NME_Assets_Name_Current_Insert]
ON [org].[AS_NME_Assets_Name_Current]
INSTEAD OF INSERT
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(EXISTS(SELECT TOP 1 1 FROM [inserted] [i] WHERE [i].[AS_NME_ID] IS NOT NULL))
		RAISERROR('INSERT into the identity column ''AS_NME_ID'' is not allowed.', 16, 1) WITH NOWAIT;

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @NME_Posit TABLE
	(
		[AS_NME_ID]					INT		NOT	NULL,
		[AS_NME_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([AS_NME_ID] ASC),
		UNIQUE CLUSTERED([AS_NME_Checksum] ASC)
	);

	INSERT INTO [org].[AS_NME_Assets_Name_Posit]
	(
		[AS_NME_AS_ID],

		[AS_NME_AssetTag],
		[AS_NME_AssetName],
		[AS_NME_AssetDetail],

		[AS_NME_ChangedAt]
	)
	OUTPUT
		[inserted].[AS_NME_ID],
		[inserted].[AS_NME_Checksum]
	INTO
		@NME_Posit
		(
			[AS_NME_ID],
			[AS_NME_Checksum]
		)
	SELECT
		[i].[AS_NME_AS_ID],

		[i].[AS_NME_AssetTag],
		[i].[AS_NME_AssetName],
		[i].[AS_NME_AssetDetail],

		COALESCE([i].[AS_NME_ChangedAt], @Now)
	FROM
		[inserted]		[i];

	INSERT INTO [org].[AS_NME_Assets_Name_Annex]
	(
		[AS_NME_ID],
		[AS_NME_PositedBy],
		[AS_NME_PositedAt],
		[AS_NME_PositReliability]
	)
	SELECT
		[p].[AS_NME_ID],
		COALESCE([i].[AS_NME_PositedBy], @PositedBy),
		COALESCE([i].[AS_NME_PositedAt], @Now),
		COALESCE([i].[AS_NME_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@NME_Posit		[p]
			ON	[p].[AS_NME_Checksum]	= [i].[AS_NME_Checksum];

END;
GO

CREATE TRIGGER [org].[AS_NME_Assets_Name_Current_Update]
ON [org].[AS_NME_Assets_Name_Current]
INSTEAD OF UPDATE
AS
BEGIN

	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	IF(UPDATE([AS_NME_ID]))
		RAISERROR('Cannot update the identity column ''AS_NME_ID''.', 16, 1);

	IF(UPDATE([AS_NME_AS_ID]))
		RAISERROR('Cannot update the foreign key identity column ''AS_NME_AS_ID''.', 16, 1);

	IF(UPDATE([AS_NME_Checksum]))
		RAISERROR('Cannot update the computed column ''AS_NME_Checksum''.', 16, 1);

	IF(UPDATE([AS_NME_PositReliable]))
		RAISERROR('Cannot update the computed column ''AS_NME_PositReliable''.', 16, 1);

	DECLARE @Reliability	INT = 50;
	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	DECLARE @NME_Posit TABLE
	(
		[AS_NME_ID]					INT		NOT	NULL,
		[AS_NME_Checksum]			INT		NOT	NULL,
		PRIMARY KEY([AS_NME_ID] ASC),
		UNIQUE CLUSTERED([AS_NME_Checksum] ASC)
	);

	INSERT INTO [org].[AS_NME_Assets_Name_Posit]
	(
		[AS_NME_AS_ID],

		[AS_NME_AssetTag],
		[AS_NME_AssetName],
		[AS_NME_AssetDetail],

		[AS_NME_ChangedAt]
	)
	OUTPUT
		[inserted].[AS_NME_ID],
		[inserted].[AS_NME_Checksum]
	INTO
		@NME_Posit
		(
			[AS_NME_ID],
			[AS_NME_Checksum]
		)
	SELECT
		[i].[AS_NME_AS_ID],

		[i].[AS_NME_AssetTag],
		[i].[AS_NME_AssetName],
		[i].[AS_NME_AssetDetail],

		COALESCE([i].[AS_NME_ChangedAt], @Now)
	FROM
		[inserted]		[i]
	WHERE	UPDATE([AS_NME_AssetTag])
		OR	UPDATE([AS_NME_AssetName])
		OR	UPDATE([AS_NME_AssetDetail]);

	INSERT INTO [org].[AS_NME_Assets_Name_Annex]
	(
		[AS_NME_ID],
		[AS_NME_PositedBy],
		[AS_NME_PositedAt],
		[AS_NME_PositReliability]
	)
	SELECT
		[p].[AS_NME_ID],
		COALESCE([i].[AS_NME_PositedBy], @PositedBy),
		COALESCE([i].[AS_NME_PositedAt], @Now),
		COALESCE([i].[AS_NME_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	INNER JOIN
		@NME_Posit		[p]
			ON	[p].[AS_NME_Checksum]	= BINARY_CHECKSUM([i].[AS_NME_AssetTag], [i].[AS_NME_AssetName], [i].[AS_NME_AssetDetail]);

	INSERT INTO [org].[AS_NME_Assets_Name_Annex]
	(
		[AS_NME_ID],
		[AS_NME_PositedBy],
		[AS_NME_PositedAt],
		[AS_NME_PositReliability]
	)
	SELECT
		[i].[AS_NME_ID],
		COALESCE([i].[AS_NME_PositedBy], @PositedBy),
		COALESCE([i].[AS_NME_PositedAt], @Now),
		COALESCE([i].[AS_NME_PositReliability], @Reliability)
	FROM
		[inserted]		[i]
	WHERE	NOT(UPDATE([AS_NME_AssetTag])
			OR	UPDATE([AS_NME_AssetName])
			OR	UPDATE([AS_NME_AssetDetail]));

END;
GO

CREATE TRIGGER [org].[AS_NME_Assets_Name_Current_Delete]
ON [org].[AS_NME_Assets_Name_Current]
INSTEAD OF DELETE
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Now			DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy		INT;
	EXECUTE @PositedBy		= [posit].[Return_PositorId];

	INSERT INTO [org].[AS_NME_Assets_Name_Annex]
	(
		[AS_NME_ID],
		[AS_NME_PositedBy],
		[AS_NME_PositedAt],
		[AS_NME_PositReliability]
	)
	SELECT
		[d].[AS_NME_ID],
		@PositedBy,
		@Now,
		0
	FROM
		[deleted]	[d]
	WHERE
		[d].[AS_NME_ID]	IS NOT NULL;

END;
GO