﻿CREATE TABLE [org].[CN_ADD_Contact_Address_Posit]
(
	[CN_ADD_ID]					INT					NOT	NULL	IDENTITY(1, 1),
	[CN_ADD_CN_ID]				INT					NOT	NULL	CONSTRAINT [FK__CN_ADD_Contact_Address_Posit_PN_ID]			REFERENCES [org].[CN_Contact]([CN_ID]),

	[CN_ADD_Type_ID]			INT					NOT	NULL,
	[CN_ADD_Country_ID]			INT						NULL,
	[CN_ADD_State]				NVARCHAR(24)			NULL	CONSTRAINT [CL__CN_ADD_Contact_Address_Posit_State]			CHECK([CN_ADD_State] <> ''),
	[CN_ADD_City]				NVARCHAR(176)			NULL	CONSTRAINT [CL__CN_ADD_Contact_Address_Posit_City]			CHECK([CN_ADD_City] <> ''),
	[CN_ADD_PostalCode]			NVARCHAR(24)			NULL	CONSTRAINT [CL__CN_ADD_Contact_Address_Posit_PostalCode]	CHECK([CN_ADD_PostalCode] <> ''),
	[CN_ADD_PoBox]				NVARCHAR(16)			NULL	CONSTRAINT [CL__CN_ADD_Contact_Address_Posit_PoBox]			CHECK([CN_ADD_PoBox] <> ''),
	[CN_ADD_Address1]			NVARCHAR(256)			NULL	CONSTRAINT [CL__CN_ADD_Contact_Address_Posit_Address1]		CHECK([CN_ADD_Address1] <> ''),
	[CN_ADD_Address2]			NVARCHAR(256)			NULL	CONSTRAINT [CL__CN_ADD_Contact_Address_Posit_Address2]		CHECK([CN_ADD_Address2] <> ''),
	[CN_ADD_Address3]			NVARCHAR(256)			NULL	CONSTRAINT [CL__CN_ADD_Contact_Address_Posit_Address3]		CHECK([CN_ADD_Address3] <> ''),
	[CN_ADD_Checksum]			AS BINARY_CHECKSUM([CN_ADD_Type_ID],
									[CN_ADD_Country_ID], [CN_ADD_State], [CN_ADD_City], [CN_ADD_PostalCode], [CN_ADD_PoBox],
									[CN_ADD_Address1], [CN_ADD_Address2], [CN_ADD_Address3])
								PERSISTED			NOT	NULL,
	[CN_ADD_ChangedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__CN_ADD_Contact_Address_Posit_ChangedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[CN_ADD_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__CN_ADD_Contact_Address_Posit_RowGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__CN_ADD_Contact_Address_Posit_RowGuid]		UNIQUE NONCLUSTERED([CN_ADD_RowGuid]),
	CONSTRAINT [PK__CN_ADD_Contact_Address_Posit]		PRIMARY KEY CLUSTERED([CN_ADD_ID] ASC)
		WITH (FILLFACTOR = 95),
	CONSTRAINT [UK__CN_ADD_Contact_Address_Posit]		UNIQUE NONCLUSTERED([CN_ADD_CN_ID] ASC, [CN_ADD_ChangedAt] DESC, [CN_ADD_Checksum] ASC)
		WITH (FILLFACTOR = 95)
);