﻿CREATE VIEW [org].[CN_PHN_Contact_Phone_SK]
WITH SCHEMABINDING
AS
SELECT
	[p].[CN_PHN_ID],
	[p].[CN_PHN_CN_ID],

	[p].[CN_PHN_Type_ID],
	[p].[CN_PHN_Number],

	[p].[CN_PHN_Checksum],
	[p].[CN_PHN_ChangedAt],
	[a].[CN_PHN_PositedBy],
	[a].[CN_PHN_PositedAt],
	[a].[CN_PHN_PositReliability],
	[a].[CN_PHN_PositReliable]

FROM
	[org].[CN_PHN_Contact_Phone_Posit]	[p]
INNER JOIN
	[org].[CN_PHN_Contact_Phone_Annex]	[a]
		ON	[a].[CN_PHN_ID]		= [p].[CN_PHN_ID];
GO

CREATE UNIQUE CLUSTERED INDEX [PK__CN_PHN_PricingValue_SK]
ON [org].[CN_PHN_Contact_Phone_SK]
(
	[CN_PHN_CN_ID]				ASC,
	[CN_PHN_ChangedAt]			DESC,
	[CN_PHN_PositedAt]			DESC,
	[CN_PHN_PositedBy]			ASC,
	[CN_PHN_PositReliable]		ASC
)
WITH (FILLFACTOR = 95);