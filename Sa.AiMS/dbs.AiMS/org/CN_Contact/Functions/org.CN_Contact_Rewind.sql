﻿CREATE FUNCTION [org].[CN_Contact_Rewind]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@Reliable		BIT					= 1
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[CN].[CN_ID],
	[CN].[CN_PositedBy],
	[CN].[CN_PositedAt],

	[ADD].[CN_ADD_ID],
	[ADD].[CN_ADD_CN_ID],

	[ADD].[CN_ADD_Type_ID],
	[ADD].[CN_ADD_Country_ID],
	[ADD].[CN_ADD_State],
	[ADD].[CN_ADD_City],
	[ADD].[CN_ADD_PostalCode],
	[ADD].[CN_ADD_PoBox],
	[ADD].[CN_ADD_Address1],
	[ADD].[CN_ADD_Address2],
	[ADD].[CN_ADD_Address3],

	[ADD].[CN_ADD_Checksum],
	[ADD].[CN_ADD_ChangedAt],
	[ADD].[CN_ADD_PositedBy],
	[ADD].[CN_ADD_PositedAt],
	[ADD].[CN_ADD_PositReliability],
	[ADD].[CN_ADD_PositReliable],

	[EML].[CN_EML_ID],
	[EML].[CN_EML_CN_ID],

	[EML].[CN_EML_Type_ID],
	[EML].[CN_EML_Address],

	[EML].[CN_EML_Checksum],
	[EML].[CN_EML_ChangedAt],
	[EML].[CN_EML_PositedBy],
	[EML].[CN_EML_PositedAt],
	[EML].[CN_EML_PositReliability],
	[EML].[CN_EML_PositReliable],

	[PHN].[CN_PHN_ID],
	[PHN].[CN_PHN_CN_ID],

	[PHN].[CN_PHN_Type_ID],
	[PHN].[CN_PHN_Number],
	
	[PHN].[CN_PHN_Checksum],
	[PHN].[CN_PHN_ChangedAt],
	[PHN].[CN_PHN_PositedBy],
	[PHN].[CN_PHN_PositedAt],
	[PHN].[CN_PHN_PositReliability],
	[PHN].[CN_PHN_PositReliable]

FROM
	[org].[CN_Contact]																[CN]

LEFT OUTER JOIN
	[org].[CN_ADD_Contact_Address_RW](@ChangedBefore, @PositedBefore)				[ADD]
		ON	[ADD].[CN_ADD_CN_ID]			= [CN].[CN_ID]
		AND	[ADD].[CN_ADD_PositReliable]	= @Reliable
		AND	[ADD].[CN_ADD_ID]				= (
			SELECT TOP 1
				[ADD_s].[CN_ADD_ID]
			FROM
				[org].[CN_ADD_Contact_Address_RW](@ChangedBefore, @PositedBefore)	[ADD_s]
			WHERE
				[ADD_s].[CN_ADD_CN_ID]		= [ADD].[CN_ADD_CN_ID]
			ORDER BY
				[ADD_s].[CN_ADD_ChangedAt]	DESC,
				[ADD_s].[CN_ADD_PositedAt]	DESC
			)

LEFT OUTER JOIN
	[org].[CN_EML_Contact_EMail_RW](@ChangedBefore, @PositedBefore)					[EML]
		ON	[EML].[CN_EML_CN_ID]			= [CN].[CN_ID]
		AND	[EML].[CN_EML_PositReliable]	= @Reliable
		AND	[EML].[CN_EML_ID]				= (
			SELECT TOP 1
				[EML_s].[CN_EML_ID]
			FROM
				[org].[CN_EML_Contact_EMail_RW](@ChangedBefore, @PositedBefore)		[EML_s]
			WHERE
				[EML_s].[CN_EML_CN_ID]		= [EML].[CN_EML_CN_ID]
			ORDER BY
				[EML_s].[CN_EML_ChangedAt]	DESC,
				[EML_s].[CN_EML_PositedAt]	DESC
			)

LEFT OUTER JOIN
	[org].[CN_PHN_Contact_Phone_RW](@ChangedBefore, @PositedBefore)					[PHN]
		ON	[PHN].[CN_PHN_CN_ID]			= [CN].[CN_ID]
		AND	[PHN].[CN_PHN_PositReliable]	= @Reliable
		AND	[PHN].[CN_PHN_ID]				= (
			SELECT TOP 1
				[PHN_s].[CN_PHN_ID]
			FROM
				[org].[CN_PHN_Contact_Phone_RW](@ChangedBefore, @PositedBefore)		[PHN_s]
			WHERE
				[PHN_s].[CN_PHN_CN_ID]		= [PHN].[CN_PHN_CN_ID]
			ORDER BY
				[PHN_s].[CN_PHN_ChangedAt]	DESC,
				[PHN_s].[CN_PHN_PositedAt]	DESC
			);