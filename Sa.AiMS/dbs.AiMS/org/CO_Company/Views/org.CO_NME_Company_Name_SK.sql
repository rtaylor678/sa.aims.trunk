﻿CREATE VIEW [org].[CO_NME_Company_Name_SK]
WITH SCHEMABINDING
AS
SELECT
	[p].[CO_NME_ID],
	[p].[CO_NME_CO_ID],

	[p].[CO_NME_CompanyTag],
	[p].[CO_NME_CompanyName],
	[p].[CO_NME_CompanyDetail],

	[p].[CO_NME_Checksum],
	[p].[CO_NME_ChangedAt],
	[a].[CO_NME_PositedBy],
	[a].[CO_NME_PositedAt],
	[a].[CO_NME_PositReliability],
	[a].[CO_NME_PositReliable]

FROM
	[org].[CO_NME_Company_Name_Posit]	[p]
INNER JOIN
	[org].[CO_NME_Company_Name_Annex]	[a]
		ON	[a].[CO_NME_ID]		= [p].[CO_NME_ID];
GO

CREATE UNIQUE CLUSTERED INDEX [PK__CO_NME_Company_Name_SK]
ON [org].[CO_NME_Company_Name_SK]
(
	[CO_NME_CO_ID]				ASC,
	[CO_NME_ChangedAt]			DESC,
	[CO_NME_PositedAt]			DESC,
	[CO_NME_PositedBy]			ASC,
	[CO_NME_PositReliable]		ASC
)
WITH (FILLFACTOR = 95);
GO